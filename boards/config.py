GROUP_ADMINISTRATOR = 'Administrator'
GROUP_KANBAN_MASTER = 'Kanban master'
GROUP_PRODUCT_OWNER = 'Product owner'
GROUP_DEVELOPER = 'Developer'

DATE_FORMAT = '%d.%m.%Y'
