from django.core.management.base import BaseCommand

from boards import models


class Command(BaseCommand):
    args = ''
    help = 'Deletes all data from database'

    def handle(self, *args, **options):
        models.Comment.objects.all().delete()
        models.Task.objects.all().delete()
        models.Card.objects.all().delete()
        models.Column.objects.all().delete()
        models.Project.objects.all().delete()
        models.Board.objects.all().delete()
        models.UserTeam.objects.all().delete()
        models.Team.objects.all().delete()
        models.User.objects.all().delete()
        models.Group.objects.all().delete()
