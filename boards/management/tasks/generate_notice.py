from django.core.mail import send_mail
from datetime import datetime

from boards.models import Board, GROUP_KANBAN_MASTER


def check_all_boards():
    for board in Board.objects.filter(is_active=True).all():
        check_expired_cards(board.pk)


def check_expired_cards(board_id):
    board = Board.objects.filter(pk=board_id).first()

    if not board.notice_interval:
        return

    msg = "\nReport for Board: '{0:s}' on day {1:s}. \n".format(board.name,
                                                                str(datetime.date(datetime.now())))
    msg += 'Following cards are due to be finished in the next {0:d} days: \n' \
        .format(board.notice_interval)

    for project in board.project_set.filter(is_active=True):
        cards = [card for card in project.card_set.all() if card.deadline and abs(
            (card.deadline - datetime.date(datetime.now())).days) <= board.notice_interval]

        if not cards:
            continue

        msg += '\nProject: %s' % project.name
        msg += '\n--------------------------------------'

        for card in cards:
            msg += "\nCard title:\t %s " \
                   "\nDeveloper:\t %s" \
                   "\nDeadline:\t %s\n" % (
                       card.name, card.assignee.get_full_name(), card.deadline)

        kanban_master = project.team.userteam_set.filter(group__name=GROUP_KANBAN_MASTER).first()

        print kanban_master
        print msg

        if kanban_master:
            send_mail('Notification on due cards for %s board' % board.name, msg,
                      'kanban.notice@gmail.com',
                      [kanban_master.user.email], fail_silently=False)