# -*- coding: utf-8 -*-

from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _


PASSWORD_MIN_LEN = 4


class LettersAndWhitespaceRegexValidator(RegexValidator):
    regex = ur'^[a-zA-ZàáâäãåąćęèéêëìíîïłńòóôöõøùúûüÿýżźñçčšžđÀÁÂÄÃÅĄĆĘÈÉÊËÌÍÎÏŁŃÒÓÔÖÕØÙÚÛÜŸÝŻŹÑß' \
            ur'ÇŒÆČŠŽ∂ðĐ\s]*$'


class LastNameValidator(LettersAndWhitespaceRegexValidator):
    message = _('Only letters allowed for first name.')


class FirstNameValidator(LettersAndWhitespaceRegexValidator):
    message = _('Only letters allowed for last name.')


class PasswordRegexValidator(RegexValidator):
    regex = r'^[A-Za-z0-9@#$%^&+=]*$'
    message = _('Please enter a valid password.')
