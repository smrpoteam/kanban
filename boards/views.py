import json
import datetime
from copy import deepcopy

from django.views.generic import View, TemplateView, ListView, CreateView
from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import Q

from braces.views import LoginRequiredMixin, CsrfExemptMixin
from django.views.generic.edit import UpdateView
from boards.forms import ColumnMoveRuleForm, TaskForm

from .models import Board, Project, Column, Card, Team, User, CardHistory, MoveColumnRule, Task
from .forms import ColumnForm

from boards.config import GROUP_PRODUCT_OWNER, GROUP_KANBAN_MASTER,\
    GROUP_ADMINISTRATOR, GROUP_DEVELOPER

from boards.management.tasks.generate_notice import check_expired_cards

DEFAULT_CRITICAL_CARD_THRESHOLD = -1


class BoardsView(LoginRequiredMixin, View):
    def get(self, request):
        user = request.user
        boards_kanban = []

        if user.can_be_admin:
            data = [{'board': b, 'kanbanmaster': False} for b in Board.objects.all()]
        else:
            teams_kanban = user.team_set.filter(userteam__group__name=GROUP_KANBAN_MASTER)
            #teams_other = user.team_set.exclude(userteam__group__name=GROUP_KANBAN_MASTER)
            ROLES = [GROUP_ADMINISTRATOR, GROUP_DEVELOPER, GROUP_PRODUCT_OWNER]
            teams_other = []
            for role in ROLES:
                teams = list(user.team_set.filter(userteam__group__name=role))
                for team in teams:
                    teams_other.append(team)

            # TODO: Check if board is active something like this
            # boards_kanban = list(self._get_team_boards(teams_kanban).filter(is_active=True))
            boards_kanban = list(self._get_team_boards(teams_kanban))

            # -------------
            my_boards = list(Board.objects.filter(is_active=True, author=user))
            boards_kanban = set(boards_kanban)
            for board in my_boards:
                boards_kanban.add(board)
            boards_kanban = list(boards_kanban)
            # -------------

            # TODO: Check if board is active something like this
            # boards_other = list(self._get_team_boards(teams_other)).filter(is_active=True))
            boards_other = list(self._get_team_boards(teams_other))
            boards_other = [b for b in boards_other if b not in boards_kanban]

            data_kanban = [{'board': b, 'kanbanmaster': True} for b in boards_kanban]
            data_other = [{'board': b, 'kanbanmaster': False} for b in boards_other]
            data = sorted(data_kanban + data_other, key=lambda el: el['board'].name.upper())

        params = {'data': data, 'exist_kanbanmaster': boards_kanban != []}
        return render(request, 'boards/boards.html', params)

    def _get_team_boards(self, team):
        projects = Project.objects.filter(team__in=team).distinct()
        return Board.objects.filter(project__in=projects).distinct()


class CreateBoardView(CsrfExemptMixin, LoginRequiredMixin, View):
    def post(self, request):
        table_name = request.POST['board_name']
        if Board.objects.filter(name=table_name, is_active=True).count() == 0:
            board = Board(name=table_name)
            board.author = request.user
            board.save()
            return render(request, 'boards/editBoard.html', {'board': board})
        else:
            #board already exist
            boards = Board.objects.filter(is_active='True')
            return render(request, 'boards/boards.html',
                          {'boards': boards, 'error_board_exist': True})
        """
        elif Board.objects.filter(name=table_name, is_active=False).count() > 0:
            board = Board.objects.get(name=table_name, is_active=False)
            board.is_active = True
            board.save()
            return render(request, 'boards/editBoard.html', {'board': board, 'teams': teams})
        """


class EditBoardView(CsrfExemptMixin, LoginRequiredMixin, View):
    def get(self, request, pk):
        if pk is not None:
            board = Board.objects.get(id=pk)
        else:
            return render(request, 'boards/boards.html')

        if board is not None:
            cols_subcols = get_columns(board.id)
            attribute_columns = get_attribute_columns(board.id)
            selected_attributes = get_selected_attribute_columns(board.id)
            params = {'board': board, 'table_data': cols_subcols,
                      'attribute_columns': attribute_columns,
                      'selected_attributes': selected_attributes}
            return render(request, 'boards/editBoard.html', params)
        else:
            return render(request, 'boards/boards.html')

    def post(self, request):
        board_id = request.POST.get('board_id')
        new_name = request.POST.get('board_name')
        interval = request.POST.get('notice_interval')
        board = None
        if board_id and interval:
            board = get_object_or_404(Board, pk=board_id)
            board.notice_interval = interval
            board.save()
        if board_id and new_name:
            board = get_object_or_404(Board, pk=board_id)
            board.name = new_name
            board.save()
        if board is not None:
            cols_subcols = get_columns(board.id)
            attribute_columns = get_attribute_columns(board.id)
            selected_attributes = get_selected_attribute_columns(board.id)
            params = {'board': board, 'table_data': cols_subcols,
                      'attribute_columns': attribute_columns,
                      'selected_attributes': selected_attributes}
            return render(request, 'boards/editBoard.html', params)
        else:
            return render(request, 'boards/boards.html')


class ShowBoardView(CsrfExemptMixin, LoginRequiredMixin, View):
    def get(self, request, pk, critical_card_threshold=DEFAULT_CRITICAL_CARD_THRESHOLD,
            critical_card_error=False, form=None):
        user = request.user
        board = get_object_or_404(Board, pk=pk)
        table_data = None
        if board:
            table_data = get_columns(board.id)

        product_owner_or_kanban_master = []

        for project in board.project_set.filter(is_active=True):
            if user.is_kanban_master(project) or \
            user.is_product_owner(project):
                product_owner_or_kanban_master.append(project)

        done_columns = []
        if board.column_set.exists() and critical_card_threshold != 0:
            done_columns = [board.post_acceptance_columns.pop()]

        default_threshold = False
        if critical_card_threshold == DEFAULT_CRITICAL_CARD_THRESHOLD:
            default_threshold = True

        params = {
            'form': form,
            'table_data': table_data,
            'critical_card_threshold': critical_card_threshold,
            'default_threshold': default_threshold,
            'critical_card_error': critical_card_error,
            'end_boundary_and_after_columns': done_columns,
            'board': board,
            'project_kbm_po': product_owner_or_kanban_master,
            'projects': json.dumps([p.pk for p in board.project_set.filter(is_active=True)]),
        }
        return render(request, 'boards/show_board.html', params)

    def post(self, request, pk):
        num_of_days = request.POST['num_of_days']
        show_board = ShowBoardView()
        if num_of_days.isdigit():
            return show_board.get(request, pk=pk, critical_card_threshold=int(num_of_days))
        else:
            return show_board.get(request, pk=pk, critical_card_error=True)


class ShowColumnMixin(object):
    def get_context_data(self, **kwargs):
        context_data = super(ShowColumnMixin, self).get_context_data(**kwargs)
        context_data['board'] = Board.objects.get(pk=self.kwargs['board_id'])
        return context_data


class ColumnCreateView(LoginRequiredMixin, ShowColumnMixin, CreateView):
    model = Column
    form_class = ColumnForm
    template_name = 'boards/createColumn.html'

    def get_form_kwargs(self):
        kwargs = super(ColumnCreateView, self).get_form_kwargs()
        if kwargs.get('data') is not None:
            data = deepcopy(kwargs.get('data').dict())
            data.update({'board': self.kwargs["board_id"]})
            kwargs['data'] = data
        kwargs['board_id'] = self.kwargs['board_id']

        return kwargs


def get_column_children(parent_column_id):
    """
    From given column id function returns list of all children.
    """
    # find fist child
    column = Column.objects.filter(previous_column=None, parent_column_id=parent_column_id,
                                   is_active=True)
    columns = []
    if column:
        column = column[0]
        while column is not None:
            # append child and move to next column
            columns.append(column)
            column = column.next_column

    return columns


def get_columns(board_id):
    """
    Returns a list of dictionaries with keys column, children and number_of_cards.
    Column represents column object while children represents a list of objects(Column)
    which represent children.
    """
    # get first column on board
    column = Column.objects.filter(board=board_id, previous_column=None,
                                   parent_column=None, is_active=True)
    columns = []
    # if board have columns
    if column:
        column = column[0]

        while column is not None:
            children = []
            number_of_cards = 0
            # column have children
            if column.children.all():
                # get all children of column in proper order
                children = get_column_children(column.id)
                number_of_cards += get_number_of_cards(children)

            number_of_cards += len(column.card_set.filter(is_active=True))

            column_dictionary = {'column': column, 'children': children,
                                 'number_of_cards': number_of_cards}
            columns.append(column_dictionary)
            column = column.next_column

    return columns


def get_number_of_cards(children):
    """
    Return number of cards in list of children.
    """
    number = 0
    for child in children:
        number += len(child.card_set.filter(is_active=True))
    return number


def remove_attributes_from_column(col):
    if col.is_start_boundary:
        col.is_start_boundary = False
    if col.is_end_boundary:
        col.is_end_boundary = False
    if col.is_priority:
        col.is_priority = False
    if col.is_acceptance:
        col.is_acceptance = False
    return col


class ColumnDeleteView(LoginRequiredMixin, View):
    def get(self, request, pk):
        column = get_object_or_404(Column, pk=pk)
        column.is_active = False
        context = {}
        if column.has_attribute_set:
            # column has set one of the attributes. Remove this selection and show warning
            context['select_new_attribute_column'] = True
            column = remove_attributes_from_column(column)
        previous_column = None
        if column:
            board_id = column.board.id
            if column.children.all():
                # column has subcolumns - delete is not allowed
                context['delete_error_column_has_children'] = True
            else:
                if column.parent_column:
                    column.parent_column = None
                if hasattr(column, 'previous_column') and column.next_column:
                    # column has both, previous and next column
                    previous_column = get_object_or_404(Column, pk=column.previous_column.id)
                    next_column = get_object_or_404(Column, pk=column.next_column.id)
                    column.next_column = None
                    previous_column.next_column = next_column
                elif not hasattr(column, 'previous_column') and column.next_column:
                    # column does not have previous column, but has next column
                    # it is first column or first subcolumn
                    column.next_column = None
                elif hasattr(column, 'previous_column') and not column.next_column:
                    # column has previous column, but does not have next column
                    # it is last column or last subcolumn
                    previous_column = get_object_or_404(Column, pk=column.previous_column.id)
                    previous_column.next_column = None

            board = get_object_or_404(Board, pk=board_id)
            column.save()
            if previous_column:
                previous_column.save()
        table_data = None
        if board:
            table_data = get_columns(board.id)
            selected_attributes = get_selected_attribute_columns(board.id)
            attribute_columns = get_attribute_columns(board.id)
            context['selected_attributes'] = selected_attributes
            context['attribute_columns'] = attribute_columns
        context['table_data'] = table_data
        context['board'] = board
        if 'select_new_attribute_column' in context and context['select_new_attribute_column']:
            # redirect to the special attribute selection form
            return render(request, 'boards/attribute_rechoose.html', context)
        else:
            return render(request, 'boards/editBoard.html', context)


class ColumnEditView(LoginRequiredMixin, View):
    def get(self, request, pk):
        column = get_object_or_404(Column, pk=pk)
        context = {}
        if column:
            context['column'] = column

        return render(request, 'boards/edit_column.html', context)

    def post(self, request):
        column_id = request.POST['column_id']
        column = get_object_or_404(Column, pk=column_id)
        context = {}
        save_column = True
        if column:
            # update name
            if request.POST.get('name'):
                column.name = request.POST.get('name')
            # update WIP
            if request.POST.get('wip'):
                wip = int(request.POST.get('wip'))  # wip value from form
                if column.has_children():
                    # column has children - check that new wip is greater or equal than
                    # largest child wip
                    child = column.get_largest_wip_child
                    if child:
                        if wip and child.wip > wip:
                            save_column = False
                            context['error_child_wip'] = True
                elif column.parent_column:
                    # column has parent - check if new wip is not larger than parent wip
                    parent = column.parent_column
                    if parent.wip and parent.wip < wip:
                        save_column = False
                        context['error_parent_wip'] = True

                if save_column:
                    # check if wip is changed and write history
                    if int(wip) != column.wip:
                        card_history = CardHistory()
                        card_history.user = request.user
                        card_history.column_from = column
                        card_history.column_to = column
                        if wip < column.card_count:
                            # write violation to database
                            card_history.action = CardHistory.ACTION_COLUMN_WIP_VIOLATION
                        else:
                            card_history.action = CardHistory.ACTION_COLUMN_WIP_CHANGED
                        column.wip = wip
                        card_history.save()
            # update color
            if request.POST.get('color'):
                if len(request.POST.get('color')) == 7:
                    column.color = request.POST.get('color')[1:]
                elif len(request.POST.get('color')) == 6:
                    column.color = request.POST.get('color')
            if save_column:
                column.save()

            context['board'] = column.board
            context['column'] = column
            if save_column:
                table_data = None
                if column.board:
                    table_data = get_columns(column.board.id)
                    selected_attributes = get_selected_attribute_columns(column.board.id)
                    attribute_columns = get_attribute_columns(column.board.id)
                    context['selected_attributes'] = selected_attributes
                    context['attribute_columns'] = attribute_columns
                context['table_data'] = table_data

                return render(request, 'boards/editBoard.html', context)
            return render(request, 'boards/edit_column.html', context)

    def update_column(self, column, parent_column, next_column):
        parent_prev = None
        if column.parent_column:
            parent_prev = get_object_or_404(Column, pk=column.parent_column.id)
        next_prev = None
        if column.next_column:
            next_prev = get_object_or_404(Column, pk=column.next_column.id)

        prev_last = None
        if not parent_column and not next_column:
            prev_last_q = Column.objects.filter(board=column.board_id, parent_column=None,
                                                next_column=None, is_active=True)
            if prev_last_q:
                prev_last = prev_last_q[0]

        if next_column != next_prev:
            # next_column was changed
            new_parent = None
            if next_column:
                if next_column.parent_column:
                    new_parent = next_column.parent_column
            else:
                if parent_column:
                    new_parent = parent_column
            prev_left = None
            if hasattr(column, 'previous_column'):
                prev_left = get_object_or_404(Column, pk=column.previous_column.id)
            if prev_left:
                column.next_column = None
                column.save()
                prev_left.next_column = next_prev
                prev_left.save()
            else:
                column.next_column = None
                column.save()

            next_left = None
            if next_column and hasattr(next_column, 'previous_column'):
                next_left = get_object_or_404(Column, pk=next_column.previous_column.id)
            if next_left:
                next_left.next_column = column
                next_left.save()

            column.next_column = next_column
            column.parent_column = new_parent
        elif parent_column != parent_prev:
            # parent column was changed
            if parent_column:
                if len(parent_column.children.all()) > 0:
                    # new parent has at least one child, find last...
                    last_child = Column.objects.filter(parent_column=parent_column.id,
                                                       is_active=True, next_column=None)
                    if last_child:
                        last_child[0].next_column = column
                        last_child[0].save()
            if parent_prev:
                if len(parent_prev.children.all()) > 1:
                    # old parent has at least one more child, find left and right
                    left_child = None
                    if hasattr(column, 'previous_column'):
                        left_child = get_object_or_404(Column, pk=column.previous_column.id)
                    right_child = None
                    if column.next_column:
                        right_child = get_object_or_404(Column, pk=column.next_column.id)
                    if left_child:
                        column.next_column = None
                        column.save()
                        left_child.next_column = right_child
                        left_child.save()
                    elif right_child:
                        column.next_column = None
                        column.save()
            column.parent_column = parent_column
        if not column.parent_column and not column.next_column:
            if prev_last:
                if prev_last != column:
                    prev_last.next_column = column
                    prev_last.save()

        return column


def get_attribute_columns(board_id):
    """
    Get columns for boundary attributes, acceptance testing attributes and high priority cards.
    """
    filtered_cols = Column.objects.filter(is_active=True, board_id=board_id)
    return [col for col in filtered_cols if not col.has_children()]


def get_selected_attribute_columns(board_id):
    start_boundary = Column.objects.filter(is_active=True, board_id=board_id,
                                           is_start_boundary=True)
    if start_boundary:
        start_boundary = start_boundary[0]
    else:
        start_boundary = None
    end_boundary = Column.objects.filter(is_active=True, board_id=board_id, is_end_boundary=True)
    if end_boundary:
        end_boundary = end_boundary[0]
    else:
        end_boundary = None
    priority = Column.objects.filter(is_active=True, board_id=board_id, is_priority=True)
    if priority:
        priority = priority[0]
    else:
        priority = None
    acceptance = Column.objects.filter(is_active=True, board_id=board_id, is_acceptance=True)
    if acceptance:
        acceptance = acceptance[0]
    else:
        acceptance = None
    return {'start_boundary': start_boundary, 'end_boundary': end_boundary, 'priority': priority,
            'acceptance': acceptance}


class SetColumnsAttributesView(LoginRequiredMixin, View):
    def post(self, request):
        board = None
        if request.POST['board_id']:
            board_id = request.POST['board_id']
            board = get_object_or_404(Board, pk=board_id)
            attribute_columns = get_selected_attribute_columns(board_id)
            # first check if start and end boundary columns are not the same
            errors = False
            errors_dict = {}
            same_start_end = False
            if request.POST['start_boundary_column'] and request.POST['end_boundary_column']:
                start_column = get_object_or_404(Column, pk=request.POST['start_boundary_column'])
                end_column = get_object_or_404(Column, pk=request.POST['end_boundary_column'])
                if request.POST['start_boundary_column'] == request.POST['end_boundary_column']:
                    # columns are the same
                    #errors = True
                    #errors_dict['error_start_end_same'] = True
                    same_start_end = True
                if not same_start_end and not self.check_start_end(start_column, end_column):
                    # end column is before start column
                    errors = True
                    errors_dict['end_before_start'] = True
            if not errors:
                self.update_attribute_column(request, attribute_columns, 'start_boundary_column',
                                             'start_boundary')
                self.update_attribute_column(request, attribute_columns, 'end_boundary_column',
                                             'end_boundary')
                self.update_attribute_column(request, attribute_columns, 'priority_column',
                                             'priority')
                self.update_attribute_column(request, attribute_columns, 'acceptance_column',
                                             'acceptance')
        if board is not None:
            cols_subcols = get_columns(board.id)
            attribute_columns = get_attribute_columns(board.id)
            selected_attributes = get_selected_attribute_columns(board.id)
            params = {'board': board, 'table_data': cols_subcols,
                      'attribute_columns': attribute_columns,
                      'selected_attributes': selected_attributes}
            if errors:
                params['errors'] = errors_dict
            return render(request, 'boards/editBoard.html', params)
        else:
            return render(request, 'boards/boards.html')

    def update_attribute_column(self, request, attribute_columns, form_column_name, old_column_name):
        if request.POST[form_column_name]:
            # start boundary is selected - check if it differs with the previous one
            column_id = int(request.POST[form_column_name])
            if not attribute_columns[old_column_name] or (attribute_columns[old_column_name]
                            and attribute_columns[old_column_name].id != column_id):
                column = get_object_or_404(Column, pk=column_id)
                self.set_column_attribute(column, old_column_name, True)
                # check if start boundary column was selected before
                if attribute_columns[old_column_name]:
                    column_old = get_object_or_404(Column, pk=attribute_columns[old_column_name].id)
                    self.set_column_attribute(column_old, old_column_name, False)
        else:
            # start boundary is not selected
            if attribute_columns[old_column_name]:
                # start boundary was selected - remove it
                column_old = attribute_columns[old_column_name]
                self.set_column_attribute(column_old, old_column_name, False)

    def set_column_attribute(self, column, attribute, value):
        if attribute == 'start_boundary':
            column.is_start_boundary = value
            column.save()
        if attribute == 'end_boundary':
            column.is_end_boundary = value
            column.save()
        if attribute == 'priority':
            column.is_priority = value
            column.save()
        if attribute == 'acceptance':
            column.is_acceptance = value
            column.save()

    def check_start_end(self, start_column, end_column):
        if start_column.id == end_column.id:
            # end column was found - correct
            return True
        if not start_column.next_column and not start_column.parent_column:
            # came to the end and end column was not found - incorrect
            return False
        if start_column.next_column:
            if start_column.next_column.has_children():
                return self.check_start_end(start_column.next_column.get_first_child(), end_column)
            else:
                return self.check_start_end(start_column.next_column, end_column)
        if start_column.parent_column.next_column:
            first_child = start_column.parent_column.next_column.get_first_child()
            if first_child:
                return self.check_start_end(first_child, end_column)
            else:
                return self.check_start_end(start_column.parent_column.next_column, end_column)
        else:
            # we came to the end (subcolumns and columns) - incorrect
            return False


class MoreOptionsView(LoginRequiredMixin, View):
    def get(self, request, pk):
        card = get_object_or_404(Card, pk=pk)
        user = request.user
        if user.is_kanban_master(card.project_id):
            return render(request, 'boards/more_options.html', {'card': card})
        else:
            show_board = ShowBoardView()
            return show_board.get(request, card.project.board.id)

    def post(self, request):
        card = get_object_or_404(Card, pk=request.POST['card_id'])
        user = request.user
        if not user.is_kanban_master(card.project_id):
            return render(request, 'boards/more_options.html', {"card": card, "error": True})
        else:
            card.is_priority = False
            card.is_size = False
            card.is_deadline = False
            if request.POST.get('priority'):
                card.is_priority = True
                print "priority"
            if request.POST.get('size'):
                card.is_size = True
                print "size"
            if request.POST.get('deadline'):
                card.is_deadline = True
                print "deadline"
            card.save()
            show_board = ShowBoardView()
            return show_board.get(request, card.project.board.id)


class CopyBoardView(LoginRequiredMixin, View):
    def get(self, request):
        context = {}
        boards = Board.objects.filter(is_active=True, author=request.user)
        context['boards'] = boards

        return render(request, 'boards/copy_board.html', context)

    def post(self, request):
        #board_name = None
        #if request.POST['name']:
        board_name = request.POST.get('name')
        #old_board_id = None
        #if request.POST['old_board']:
        old_board_id = request.POST.get('old_board')
        context = {}
        valid = True
        if not board_name:
            context['error_name'] = True
            valid = False
        if not old_board_id:
            context['error_board'] = True
            valid = False
        if valid:
            # valid form - copy board
            new_board = Board()
            new_board.name = board_name
            new_board.author = request.user
            new_board.save()
            self.copy_board(new_board.id, old_board_id)
            #return render(request, 'supervision/home.html', context)
            return redirect(reverse('boards:boards'))
        else:
            # not valid form - redirect back to form
            boards = Board.objects.filter(is_active=True, author=request.user)
            context['boards'] = boards
            return render(request, 'boards/copy_board.html', context)

    def copy_board(self, new_id, old_id):
        old_board = get_object_or_404(Board, pk=old_id)
        last_old_column = old_board.last_parent
        if last_old_column:
            self._copy_column(last_old_column, new_id, None, None)

    def _copy_column(self, old_column, board_id, next_col, parent):
        column = Column()
        column.name = old_column.name
        column.wip = old_column.wip
        column.color = old_column.color
        column.board_id = board_id
        column.is_start_boundary = old_column.is_start_boundary
        column.is_end_boundary = old_column.is_end_boundary
        column.is_priority = old_column.is_priority
        column.is_acceptance = old_column.is_acceptance
        column.parent_column = parent
        column.next_column = next_col
        column.save()
        if old_column.has_children():
            last_child_old = old_column.last_child
            self._copy_column(last_child_old, board_id, None, column)
        if hasattr(old_column, 'previous_column'):
            self._copy_column(old_column.previous_column, board_id, column, column.parent_column)
        return None


class SendNotificationsView(View):
    def get(self, request, pk):
        check_expired_cards(pk)
        return redirect(reverse('boards:boards'))


class ColumnRuleCreateView(CreateView):
    form_class = ColumnMoveRuleForm
    template_name = 'boards/create_column_rule.html'

    def get_form_kwargs(self):
        kwargs = super(ColumnRuleCreateView, self).get_form_kwargs()
        kwargs['column_id'] = self.kwargs['column_id']
        return kwargs

    def get_success_url(self):
        return reverse('boards:edit_column', args=(self.object.column_origin_id,))


class DeleteColumnRuleView(View):
    def get(self, request, column_pk, rule_pk):
        rule = MoveColumnRule.objects.filter(pk=rule_pk).first()
        rule.is_active = False
        rule.save()

        return redirect(reverse('boards:edit_column', args=(column_pk,)))


class SubtasksView(LoginRequiredMixin, View):
    def get(self, request, pk):
        card = get_object_or_404(Card, pk=pk)
        user = request.user
        tasks = card.task_set.filter(is_active=True).all()
        if user.is_kanban_master(card.project_id):
            return render(request, 'boards/subtasks.html', {'card': card, 'tasks': tasks})
        else:
            show_board = ShowBoardView()
            return show_board.get(request, card.project.board.id)


class TaskDeleteView(LoginRequiredMixin, View):
    def post(self, request):
        task_id = request.POST['task_id']
        card_id = request.POST['card_id']

        task = Task.objects.filter(pk=task_id).first()
        task.is_active = False
        task.save()

        return redirect(reverse('supervision:card_detail', args=(card_id,)), request)


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    form_class = TaskForm
    template_name = 'boards/create_task.html'

    def get_form_kwargs(self):
        kwargs = super(TaskCreateView, self).get_form_kwargs()
        kwargs['card_id'] = self.kwargs['card_id']
        return kwargs

    def get_success_url(self):
        return reverse('supervision:card_detail', args=(self.object.card.pk,))


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = 'boards/update_task.html'

    def get_success_url(self):
        return reverse('supervision:card_detail', args=(self.object.card.pk,))