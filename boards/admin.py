from django.contrib import admin
from django.contrib.auth.admin import UserAdmin, UserChangeForm, UserCreationForm
from django.utils.translation import ugettext_lazy as _

import models


class CustomUserChangeForm(UserChangeForm):
    def __init__(self, *args, **kwargs):
        super(CustomUserChangeForm, self).__init__(*args, **kwargs)
        del self.fields['username']


class CustomUserCreationForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(CustomUserCreationForm, self).__init__(*args, **kwargs)
        del self.fields['username']


class CustomUserAdmin(UserAdmin):
    """
    Custom users admin without username.
    """

    add_form_template = 'admin/auth/users/add_form.html'
    change_user_password_template = None
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
        ),
    )
    form = CustomUserChangeForm
    add_form = CustomUserCreationForm
    list_display = ('email', 'first_name', 'last_name', 'is_staff')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('email', 'first_name', 'last_name', 'email')
    ordering = ('email',)
    filter_horizontal = ('groups', 'user_permissions',)


class CardHistoryAdmin(admin.ModelAdmin):
    list_display = ('action', 'time_created', 'card', 'column_from', 'column_to', 'user')
    fields = ('description', 'action', 'time_created', 'card', 'column_from', 'column_to', 'user')
    readonly_fields = ('time_created',)


admin.site.register(models.User, CustomUserAdmin)
admin.site.register(models.Column)
admin.site.register(models.Board)
admin.site.register(models.Team)
admin.site.register(models.UserTeam)
admin.site.register(models.Project)
admin.site.register(models.Card)
admin.site.register(models.CardHistory, CardHistoryAdmin)
admin.site.register(models.Task)
admin.site.register(models.Comment)
