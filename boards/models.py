from datetime import datetime

from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import Q
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.utils import timezone
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin, Group

from .config import GROUP_ADMINISTRATOR, GROUP_KANBAN_MASTER, GROUP_PRODUCT_OWNER, GROUP_DEVELOPER
from .validators import FirstNameValidator, LastNameValidator


class UserManager(BaseUserManager):
    """
    Custom UserManager for creation of users without username and with mandatory email.
    """

    def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
        now = timezone.now()
        if not email:
            raise ValueError('Users must have an email address')
        email = self.normalize_email(email)
        user = self.model(email=email, is_staff=is_staff, is_active=True, is_superuser=is_superuser,
                          last_login=now, date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        return self._create_user(email, password, False, False, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True, **extra_fields)


class AbstractUser(AbstractBaseUser, PermissionsMixin):
    """
    Custom AbstractUser model without username and with required unique email.
    """

    first_name = models.CharField(_('first name'), max_length=30, blank=True,
                                  validators=[FirstNameValidator()])
    last_name = models.CharField(_('last name'), max_length=30, blank=True,
                                 validators=[LastNameValidator()])
    email = models.EmailField(_('email address'), unique=True)
    is_staff = models.BooleanField(
        _('staff status'), default=False, help_text=_('Designates whether the users can log into '
                                                      'this admin site.'))
    is_active = models.BooleanField(
        _('active'), default=True,
        help_text=_('Designates whether this users should be treated as active. Unselect this '
                    'instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    class Meta:
        verbose_name = _('users')
        verbose_name_plural = _('users')
        abstract = True

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the users."
        return self.first_name

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])


class User(AbstractUser):
    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)
        self._is_admin = None
        self._is_kanban_master = None
        self._is_product_owner = None
        self._is_developer = None

    def get_absolute_url(self):
        return reverse('supervision:user_detail', kwargs={'pk': self.pk})

    def __unicode__(self):
        return '%s %s' % (self.first_name, self.last_name)

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        self._is_admin = None
        self._is_kanban_master = None
        self._is_product_owner = None
        self._is_developer = None

    @property
    def can_be_admin(self):
        return self.groups.filter(name=GROUP_ADMINISTRATOR).exists()

    @property
    def can_be_kanban_master(self):
        return self.groups.filter(name=GROUP_KANBAN_MASTER).exists()

    @property
    def can_be_product_owner(self):
        return self.groups.filter(name=GROUP_PRODUCT_OWNER).exists()

    @property
    def can_be_developer(self):
        return self.groups.filter(name=GROUP_DEVELOPER).exists()

    def is_admin(self, project):
        if self._is_admin is not None:
            return self._is_admin
        self._is_admin = self._is_in_group_on_project(GROUP_ADMINISTRATOR, project)
        return self._is_admin

    def is_kanban_master(self, project):
        if self._is_kanban_master is not None:
            return self._is_kanban_master
        self._is_kanban_master = self._is_in_group_on_project(GROUP_KANBAN_MASTER, project)
        return self._is_kanban_master

    def is_product_owner(self, project):
        if self._is_product_owner is not None:
            return self._is_product_owner
        self._is_product_owner = self._is_in_group_on_project(GROUP_PRODUCT_OWNER, project)
        return self._is_product_owner

    def is_developer(self, project):
        if self._is_developer is not None:
            return self._is_developer
        self._is_developer = self._is_in_group_on_project(GROUP_DEVELOPER, project)
        return self._is_developer

    def _is_in_group_on_project(self, group_name, project):
        if isinstance(project, (int, str, unicode)):  # If primary key is specified
            project = Project.objects.get(pk=project)

        user_team = project.team.userteam_set.filter(group__name=group_name)
        return user_team.filter(user=self).exists()

    @property
    def boards_active(self):
        boards = Board.objects.filter(Q(project__team__users=self) | Q(author=self))
        return boards.filter(is_active=True).distinct()

    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'
        ordering = ('last_name', 'first_name')


class BaseModel(models.Model):
    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Team(BaseModel):
    name = models.CharField(max_length=100, unique=True)
    users = models.ManyToManyField(User, through='UserTeam', blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class UserTeam(BaseModel):
    user = models.ForeignKey(User)
    team = models.ForeignKey(Team)
    start_date = models.DateTimeField(auto_now_add=True)
    end_date = models.DateTimeField(blank=True, null=True)
    group = models.ForeignKey(Group)

    def save(self, *args, **kwargs):
        if not self.is_active and not self.end_date:
            self.end_date = datetime.now()

        super(UserTeam, self).save(*args, **kwargs)

    def __unicode__(self):
        return '%s - %s %s' % (self.team.name, self.user.last_name, self.user.first_name)

    class Meta:
        ordering = ('team__name', 'user__last_name', 'user__first_name')


class Board(BaseModel):
    name = models.CharField(max_length=100)
    author = models.ForeignKey(User)
    notice_interval = models.IntegerField(blank=True, null=True)

    def __init__(self, *args, **kwargs):
        super(Board, self).__init__(*args, **kwargs)
        self._pre_development_columns = None
        self._development_columns = None
        self._post_development_columns = None

    def __unicode__(self):
        return self.name

    @property
    def has_columns(self):
        return self.column_set.filter(is_active=True).exists()

    @property
    def first_column(self):
        columns = self.column_set.filter(is_active=True)
        return columns.filter(parent_column=None, previous_column=None).first()

    @property
    def last_parent(self):
        # get the most right column on the first level
        columns = self.column_set.filter(is_active=True)
        return columns.filter(parent_column=None, next_column=None).first()

    @property
    def start_boundary_column(self):
        return self.column_set.filter(is_active=True).filter(is_start_boundary=True).first()

    @property
    def end_boundary_column(self):
        return self.column_set.filter(is_active=True).filter(is_end_boundary=True).first()

    @property
    def priority_column(self):
        return self.column_set.filter(is_active=True).filter(is_priority=True).first()

    @property
    def acceptance_column(self):
        return self.column_set.filter(is_active=True).filter(is_acceptance=True).first()

    @property
    def pre_development_columns(self):
        """Active columns before start boundary column (without start boundary column)."""

        if self._pre_development_columns is not None:
            return self._pre_development_columns

        column, start_boundary_column = self.first_column, self.start_boundary_column
        if not column or not start_boundary_column:
            return None

        pre_development_columns = []
        while column != start_boundary_column:
            pre_development_columns.append(column)
            column = column.next_active_column

        self._pre_development_columns = pre_development_columns
        return pre_development_columns

    @property
    def post_acceptance_columns(self):
        """Active columns after (right from) acceptance column (without acceptance column)."""

        acceptance_col = self.acceptance_column
        if acceptance_col is None:
            return None

        post_acceptance_cols = []
        next_col = acceptance_col.next_column
        while next_col is not None:
            if next_col.children.count():
                children = next_col.get_all_children
                for child in children:
                    post_acceptance_cols.append(child)
            else:
                post_acceptance_cols.append(next_col)
            previous_col = next_col
            next_col = next_col.next_column
            if not next_col and previous_col.parent_column:
                if previous_col.parent_column.next_column:
                    next_col = previous_col.parent_column.next_column

        return post_acceptance_cols

    @property
    def last_column(self):
        last_column = self.column_set.filter(is_active=True, parent_column=None, next_column=None)
        if last_column and last_column.first().has_children:
            return self.column_set.filter(is_active=True, parent_column=last_column.first(),
                                          next_column=None).first()
        else:
            return last_column.first()

    @property
    def development_columns(self):
        """Active columns between start and end boundary columns (including start + end columns)."""

        if self._development_columns is not None:
            return self._development_columns

        column, end_boundary_column = self.start_boundary_column, self.end_boundary_column
        if not column or not end_boundary_column:
            return None

        development_columns = []
        while column != end_boundary_column:
            development_columns.append(column)
            column = column.next_active_column
        development_columns.append(column)

        self._development_columns = development_columns
        return development_columns

    @property
    def post_development_columns(self):
        """Active columns after end boundary column (without end boundary column)."""

        if self._post_development_columns is not None:
            return self._post_development_columns

        end_boundary_column = self.end_boundary_column
        if not end_boundary_column:
            return None

        post_development_columns, column = [], end_boundary_column.next_active_column
        while column:
            post_development_columns.append(column)
            column = column.next_active_column

        self._post_development_columns = post_development_columns
        return post_development_columns

    def save(self, *args, **kwargs):
        super(Board, self).save(*args, **kwargs)
        self._pre_development_columns = None
        self._development_columns = None
        self._post_development_columns = None

    class Meta:
        ordering = ('name',)


class Project(BaseModel):
    code = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    customer = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    board = models.ForeignKey(Board)
    team = models.ForeignKey(Team)

    def clean(self):
        validation_errors = {}

        if self.start_date and self.start_date > timezone.now():
            error = 'Start date has to be lower or equal than current date.'
            validation_errors.setdefault('start_date', []).append(error)
        if self.end_date and self.end_date <= timezone.now():
            error = 'End date has to be greater than current date.'
            validation_errors.setdefault('end_date', []).append(error)
        if self.start_date and self.end_date and self.end_date <= self.start_date:
            error = 'End date has to be greater than start date.'
            validation_errors.setdefault('end_date', []).append(error)

        if validation_errors:
            raise ValidationError(validation_errors)

        super(Project, self).clean()

    def get_absolute_url(self):
        return reverse('supervision:project_detail', kwargs={'pk': self.pk})

    def save(self, *args, **kwargs):
        self.full_clean()
        super(Project, self).save(*args, **kwargs)

    @property
    def has_cards(self):
        return self.card_set.filter(is_active=True).exists()

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class Column(BaseModel):
    name = models.CharField(max_length=100)
    wip = models.IntegerField(validators=[MinValueValidator(0)])
    parent_column = models.ForeignKey('self', blank=True, null=True, related_name='children')
    next_column = models.OneToOneField('self', blank=True, null=True, related_name='previous_column')
    color = models.CharField(max_length=6)
    board = models.ForeignKey(Board)
    is_start_boundary = models.BooleanField(default=False)  # start boundary column
    is_end_boundary = models.BooleanField(default=False)    # end boundary column
    is_priority = models.BooleanField(default=False)        # column with highest priority
    is_acceptance = models.BooleanField(default=False)      # acceptance testing column

    # TODO: type, permission, (ask Furst)
    def __init__(self, *args, **kwargs):
        super(Column, self).__init__(*args, **kwargs)
        self._parent_most_column = None

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('boards:editBoard', kwargs={'pk': self.board.pk})

    def has_children(self):
        if self.children:
            return len(self.children.all()) > 0

    def get_first_child(self):
        if not self.has_children():
            return None
        first_child = Column.objects.filter(is_active=True, parent_column=self,
                                            previous_column=None)
        if len(first_child) > 0:
            return first_child[0]

    @property
    def last_child(self):
        columns = Column.objects.filter(is_active=True)
        return columns.filter(parent_column=self, next_column=None).first()

    @property
    def has_attribute_set(self):
        return self.is_start_boundary or self.is_end_boundary or self.is_acceptance or \
               self.is_priority

    @property
    def previous_active_column(self):
        return self._previous_active_column(self)

    def _previous_active_column(self, column):
        """Returns first previous active column"""

        if not column:
            return None

        if hasattr(column, 'previous_column'):
            if column.previous_column.is_active:
                children = column.previous_column.children.filter(is_active=True, next_column=None)
                if children.exists():
                    return children.first()
                return column.previous_column
            else:
                return self._previous_active_column(column.previous_column)

        return self._previous_active_column(column.parent_column)

    @property
    def previous_active_columns(self):
        """Returns all active columns left to current column."""

        previous_columns, column = [], self
        while column:
            column = column.previous_active_column
            previous_columns.append(column)

        return previous_columns

    @property
    def next_active_column(self):
        return self._next_active_column(self)

    def _next_active_column(self, column):
        """Returns first next active column"""

        if not column:
            return None

        if column.next_column:
            if column.next_column.is_active:
                children = column.next_column.children.filter(is_active=True, previous_column=None)
                if children.exists():
                    return children.first()
                return column.next_column
            else:
                return self._next_active_column(column.next_column)

        return self._next_active_column(column.parent_column)

    @property
    def next_active_columns(self):
        """Returns all active columns right to current column."""
        next_columns, column = [], self
        while column:
            column = column.next_active_column
            next_columns.append(column)

        return next_columns

    @property
    def parent_most_column(self):
        if self._parent_most_column:
            return self._parent_most_column

        column = self
        while column.parent_column:
            column = column.parent_column

        self._parent_most_column = column
        return self._parent_most_column

    @property
    def card_count(self):
        return self._get_card_count(self)

    def _get_card_count(self, column):
        """
        Counts the number of cards for provided column. If column is not provided, then cards are
        counted for current column.
        """

        if not self.children.exists():
            return self.card_set.filter(is_active=True).count()

        column, counter = column or self, 0

        for child in column.children.all():
            if child.children.exists():
                counter += self._get_card_count(child)
            else:
                counter += child.card_set.filter(is_active=True).count()

        return counter

    @property
    def silver_bullet_cards(self):
        return self.card_set.filter(card_type=Card.TYPE_SILVER_BULLET).filter(is_active=True)

    @property
    def get_largest_wip_child(self):
        # returns child with the largest wip
        children_sorted = Column.objects.filter(is_active=True, parent_column=self.id, wip__gt=0)\
            .order_by('-wip')
        if children_sorted.exists():
            return children_sorted[0]
        return None

    @property
    def get_all_children(self):
        children_list = []
        child = self.get_first_child()
        while child:
            children_list.append(child)
            child = child.next_column
        return children_list

    class Meta:
        ordering = ('name',)


class MoveColumnRule(BaseModel):
    column_origin = models.ForeignKey(Column, related_name='origin_rules')
    column_target = models.ForeignKey(Column, related_name='target_rules')
    user_group = models.ForeignKey(Group)

    class Meta:
        ordering = ('user_group',)


class Card(BaseModel):
    TYPE_SILVER_BULLET = 'S'
    TYPE_REGULAR = 'R'
    COLOR_SILVER_BULLET = '#ff9933'
    COLOR_REGULAR = '#34a97b'
    COLOR_REJECTED = '#FF5C5C'
    COLOR_DEADLINE = '#4e8aea'
    PRIORITY_COULD_HAVE = 1
    PRIORITY_SHOULD_HAVE = 2
    PRIORITY_MUST_HAVE = 3

    priority = models.IntegerField(choices=[(PRIORITY_COULD_HAVE, 'Could Have'),
                                            (PRIORITY_SHOULD_HAVE, 'Should Have'),
                                            (PRIORITY_MUST_HAVE, 'Must Have')])
    deadline = models.DateField(blank=True, null=True)
    assignee = models.ForeignKey(User, blank=True, null=True)
    size = models.IntegerField(validators=[MinValueValidator(0)], default=0, blank=True, null=True)
    project = models.ForeignKey(Project)
    color = models.CharField(max_length=7)
    card_type = models.CharField(max_length=1, choices=[(TYPE_SILVER_BULLET, 'Silver Bullet'),
                                                        (TYPE_REGULAR, 'Regular')])
    column = models.ForeignKey(Column)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=500)

    is_priority = models.BooleanField(default=True)
    is_size = models.BooleanField(default=True)
    is_deadline = models.BooleanField(default=True)

    def clean(self):
        validation_errors = {}
        card = Card.objects.get(pk=self.pk) if self.pk else None

        # If new card is added, than date of deadline has to be greater than current date. If card
        # is being updated, than new date has to be also greater than current date. If the date is
        # the same as previously saved date, this means, that user didn't update deadline. In that
        # case date can stay the same.
        if self.deadline and self.deadline <= datetime.now().date() and \
                (not card or self.deadline != card.deadline):
            error = 'Date of deadline has to be greater than current date.'
            validation_errors.setdefault('deadline', []).append(error)

        if validation_errors:
            raise ValidationError(validation_errors)

        super(Card, self).clean()

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('boards:show_board', kwargs={'pk': self.project.board.pk})

    def can_update_card(self, user):
        """According to user's role he can edit cards, which are in certain columns."""

        project, column, board = self.project, self.column, self.column.board

        can_update = False
        if user.is_kanban_master(project) or user.is_product_owner(project):
            can_update = can_update or column in board.pre_development_columns
        if user.is_kanban_master(project) or user.is_developer(project):
            can_update = can_update or column in board.development_columns
        return can_update

    def can_delete_card(self, user):
        if user.is_product_owner(self.project):
            return self.column in self.column.board.pre_development_columns
        return user.is_kanban_master(self.project)

    @property
    def deadline_time(self):
        return (self.deadline - datetime.date(datetime.now())).days

    @property
    def is_done(self):
        return not self.task_set.filter(is_active=True, done=False).exists()

    class Meta:
        ordering = ('name',)


class CardHistory(models.Model):
    ACTION_CARD_CREATED = 'Card Created'
    ACTION_CARD_DELETED = 'Card deleted'
    ACTION_CARD_RESTORED = 'Card restored'
    ACTION_CARD_MOVED = 'Card moved'
    ACTION_CARD_WIP_VIOLATION = 'Card WIP violation'
    ACTION_COLUMN_WIP_CHANGED = 'Column WIP changed'
    ACTION_COLUMN_WIP_VIOLATION = 'Column WIP violated'

    description = models.CharField(max_length=500, blank=True)
    action = models.CharField(default=ACTION_CARD_MOVED, max_length=30, blank=True, choices=[
        (ACTION_CARD_CREATED, 'Card created.'), (ACTION_CARD_DELETED, 'Card deleted.'),
        (ACTION_CARD_RESTORED, 'Card restored.'), (ACTION_CARD_MOVED, 'Card moved.'),
        (ACTION_CARD_WIP_VIOLATION, 'Card WIP violated.'),
        (ACTION_COLUMN_WIP_CHANGED, 'Column WIP changed.'),
        (ACTION_COLUMN_WIP_VIOLATION, 'Column WIP changed and violated.')
    ])
    time_created = models.DateTimeField(auto_now_add=True)
    card = models.ForeignKey(Card, null=True)
    column_from = models.ForeignKey(Column, null=True, related_name='movements_from')
    column_to = models.ForeignKey(Column, null=True, related_name='movements_to')
    user = models.ForeignKey(User)

    def __unicode__(self):
        return "From: %s To: %s Time_created: %s" % \
               (self.column_from, self.column_to, self.time_created)

    class Meta:
        ordering = ('-time_created',)


class Task(BaseModel):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=500)
    size = models.IntegerField()
    assignee = models.ForeignKey(User)
    card = models.ForeignKey(Card)
    done = models.BooleanField()

    #TODO: what's up with the "task" field?

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('name', )


class Comment(BaseModel):
    task = models.ForeignKey(Task)
    text = models.CharField(max_length=500)

    def __unicode__(self):
        return self.task.name

    class Meta:
        ordering = ('task__name', )
