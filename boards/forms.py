from django import forms
from django.db.models import Q

from .models import Column, MoveColumnRule, Task, Card


class ColumnMoveRuleForm(forms.ModelForm):
    origin_column_id = forms.IntegerField()

    def __init__(self, column_id, *args, **kwargs):
        super(ColumnMoveRuleForm, self).__init__(*args, **kwargs)

        columns = Column.objects.filter(pk=column_id, is_active=True)
        origin_column = columns.first()
        children = [c.pk for c in origin_column.get_all_children]
        other_columns = Column.objects.exclude(Q(pk=column_id) | Q(pk__in=children)) \
            .filter(is_active=True, board__pk=origin_column.board.pk)

        self.fields['origin_column_id'].initial = columns.first().pk
        self.fields["column_origin"].queryset = columns
        self.fields["column_origin"].required = True
        self.fields["column_target"].queryset = other_columns
        self.fields["column_target"].required = True
        self.fields["is_active"].initial = True

    class Meta:
        model = MoveColumnRule


class TaskForm(forms.ModelForm):
    card_id = forms.IntegerField()

    def __init__(self, card_id, *args, **kwargs):
        super(TaskForm, self).__init__(*args, **kwargs)
        cards = Card.objects.filter(pk=card_id)
        self.fields['card'].queryset = cards
        self.fields['card'].initial = cards.first()
        self.fields['card_id'].required = False

    class Meta:
        model = Task
        fields = ('name', 'description', 'size', 'assignee', 'card', 'done')


class ColumnForm(forms.ModelForm):
    def __init__(self, board_id, data=None, *args, **kwargs):
        if data and data.get('color') and len(data.get('color')) == 7:
            data['color'] = data['color'][1:]

        super(ColumnForm, self).__init__(data, *args, **kwargs)
        self.fields["parent_column"].queryset = Column.objects.filter(parent_column=None,
                                                                      board=board_id,
                                                                      is_active=True,
                                                                      is_start_boundary=False,
                                                                      is_end_boundary=False,
                                                                      is_priority=False,
                                                                      is_acceptance=False)
        self.fields["next_column"].queryset = Column.objects.filter(is_active=True, board=board_id)
        self.fields["is_active"].initial = True

    def clean(self):
        # check if entered wip is less or equal than parent wip
        cleaned_data = super(ColumnForm, self).clean()
        wip = 0
        next_column = cleaned_data.get('next_column')
        parent_column = cleaned_data.get('parent_column')
        if next_column:
            if next_column.parent_column:
                wip = next_column.parent_column.wip
        elif parent_column:
            wip = parent_column.wip
        if wip > 0:
            column_wip = cleaned_data.get('wip')
            if column_wip > wip:
                error_message = "WIP must be less or equal than parent column WIP(%(w)d)" % {
                    'w': wip}
                errors = self.error_class([error_message])
                self._errors.setdefault('wip', []).extend(errors)
        return cleaned_data

    def _post_clean(self):
        old_next_error = self._errors.get('next_column')
        super(ColumnForm, self)._post_clean()

        if not old_next_error and self._errors.get('next_column'):
            del self._errors['next_column']

    def save(self, commit=True):
        previous_column = None

        if self.instance.next_column:
            next_column = Column.objects.get(pk=self.instance.next_column.pk)
            self.instance.parent_column = next_column.parent_column

            if hasattr(next_column, 'previous_column'):
                # adding column in the middle
                previous_column = next_column.previous_column
                previous_column.next_column = None
                previous_column.save()
            else:
                # adding column on the left
                self.instance.next_column = next_column
        else:
            if self.instance.parent_column:
                # add new subcolumn if there is no subcolumns yet
                parent_column = self.instance.parent_column
                #if parent_column.is_start_boundary or parent_column.is_end_boundary or parent_column.is
                previous_column = Column.objects.filter(parent_column=parent_column,
                                                        next_column=None, is_active=True)
                if previous_column:
                    previous_column = previous_column[0]
                else:
                    previous_column = None
                self.instance.next_column = None
            else:
                # parent and next columns are not selected
                # it can be first column on table or last column on first level
                last_column = Column.objects.filter(board=self.instance.board, parent_column=None,
                                                    next_column=None, is_active=True)
                if last_column:
                    # table is not empty - get last column on first level
                    previous_column = last_column[0]

        instance = super(ColumnForm, self).save(commit=commit)
        if previous_column:
            previous_column.next_column = instance
            previous_column.save()

        return self.instance

    class Meta:
        model = Column
