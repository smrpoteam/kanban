// Used for drag and drop to memorise start column id.
var startColumnId = null;

$(document).ready(function () {
    $(".narrowing").click(function () {
        // Get number of column
        var number = $(this).attr("column-number");
        // Get class name of elements in column
        var class_name = ".rc" + number;
        // Get class name of column text
        var column_text = ".ct" + number;

        // If element doesn't have sub columns
        if($(class_name).length) {
            if($(this).find("div").is(":visible")) {
                updateStatesColumn(this, class_name, column_text, true);
            } else {
                updateStatesColumn(this, class_name, column_text, false);
            }
        // If element have sub columns
        } else {
            // Proper class names for sub column elements
            var new_class_name = ".rc" + number / 10;
            var sub_class_name = ".sc" + number / 10;
            column_text = ".ct" + number / 10;

            // "Fake" hide base column
            if($(this).find("div").hasClass("fake-visible")) {
                $(this).find("div").removeClass("fake-visible");
                updateStatesSubColumn(new_class_name, sub_class_name, column_text, true);
            // "Fake" show base column
            } else {
                $(this).find("div").addClass("fake-visible");
                updateStatesSubColumn(new_class_name, sub_class_name, column_text, false);
            }
        }
    });

    // Makes cards draggable for every swim lane.
    $.each(projectIds, function(index, value) {
        $('.project-' + value).sortable({
            connectWith: ['.project-' + value],
            start: startDrag,
            update: endDrag
        });
    });

    $('.delete_card_button').on('click', showDeleteCardModal);
});

function updateStatesColumn(root, name, column, hide) {
    /*
    * Show or hide column.
    * root = current element
    * name = division which hold cards or empty division
    * column = column name reference
    * hide = true->hide; false->show
    */
    var classes = ["card-width", "narrow-column"];
    if(hide) {
        // Hide column header
        $(root).find("div").hide();
        // Hide cards or empty division
        $(name).hide();
        // Show column name vertical
        $(column).show();
    } else {
        // Show column header
        $(root).find("div").show();
        // Show cards or empty division
        $(name).show();
        // Hide column name
        $(column).hide();

        // Reverse list
        classes.reverse();
    }
    // Remove and add classes
    $(name).removeClass(classes[0]);
    $(name).addClass(classes[1]);
}

function updateStatesSubColumn(new_name, sub_name, column, hide) {
    // Index for sub column elements
    var index = 1;
    var classes = ["card-width", "narrow-column"];
    do {
        // Get new class names
        new_name = getClassSubstring(index, new_name);
        sub_name = getClassSubstring(index, sub_name);
        column = getClassSubstring(index, column);

        // Hide or show elements
        if(hide) {
            $(sub_name).find("div").show();
            $(new_name).show();
            $(column).hide();
        } else {
            $(sub_name).find("div").hide();
            $(new_name).hide();
            $(column).show();
        }
        // Update classes
        $(new_name).removeClass(classes[1]);
        $(new_name).addClass(classes[0]);

        index++;
    // Until no more sub columns
    } while($(new_name).length)
}

function getClassSubstring(index, string) {
    /*
    * Function cat off last char in class name od update
    * to new name which fits to next sub column.
    */
    string = string.substring(0, 4);
    string += index;
    return string;
}

/**
 * Function, which is called, when card starts to get dragged.
 * @param event
 * @param ui
 */
function startDrag(event, ui) {
    startColumnId = ui.item.parent().data('column-id');
}

/**
+ * Function, which is called, when card card is not dragged any more.
+ * @param event
+ * @param ui
+ */
function endDrag(event, ui) {
    var column;
    var endColumnId;

    if (!startColumnId) {
        return;
    }

    column = ui.item.parent();
    if (!column.is('ul')) {
        return
    }

    endColumnId = column.data('column-id');
    if (!endColumnId) {
        return;
    }

    if (startColumnId == endColumnId) {
        return;
    }

    var cardId = ui.item.data('card-id');
    var form = $(document.createElement('form'));
    form.attr('method', 'post');
    form.attr('action', cardMoveUrl.replace('0', cardId));
    form.append(createInputField('column', endColumnId));
    form.append($('input[name=csrfmiddlewaretoken]'));
    $('body').append(form);
    form.submit();

    startColumnId = null;
}

/**
 * Shows modal for deleting card and sets request url on form.
 * @param event
 */
function showDeleteCardModal(event) {
    var cardId = $(event.target).data('card-id');
    var form = $('#delete_card_form');

    if (!cardId || !form) {
        return;
    }

    form.attr('action', cardDeleteUrl.replace('0', cardId));
    $('#card_delete_modal').modal();
}
