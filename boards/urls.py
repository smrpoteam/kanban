from django.conf.urls import patterns, include, url
from django.contrib import admin

import views

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', views.BoardsView.as_view(), name='boards'),
    url(r'^show/(?P<pk>\d+)/$', views.ShowBoardView.as_view(), name='show_board'),
    url(r'^more/card/update/$', views.MoreOptionsView.as_view(), name='update_more_options'),
    url(r'^more/(?P<pk>\d+)/$', views.MoreOptionsView.as_view(), name='more_options'),
    url(r'^subtasks/edit/(?P<pk>\d+)/$', views.TaskUpdateView.as_view(), name='task_edit'),
    url(r'^subtasks/edit/$', views.TaskUpdateView.as_view(), name='task_edit'),
    url(r'^subtasks/delete/$', views.TaskDeleteView.as_view(), name='task_delete'),
    url(r'^subtasks/(?P<card_id>\d+)/create/$', views.TaskCreateView.as_view(), name='task_create'),
    url(r'^create/$', views.CreateBoardView.as_view(), name='createBoard'),
    url(r'^copy_board/$', views.CopyBoardView.as_view(), name='copy_board'),
    url(r'^edit/(?P<pk>\d+)/$', views.EditBoardView.as_view(), name='editBoard'),
    url(r'^edit/rename/$', views.EditBoardView.as_view(), name='rename_board'),
    url(r'^columns/(?P<board_id>\d+)/create/$', views.ColumnCreateView.as_view(),
        name='create_column'),
    url(r'^columns/(?P<pk>\d+)/delete/$', views.ColumnDeleteView.as_view(),
        name='delete_column'),
    url(r'^columns/(?P<pk>\d+)/update/$', views.ColumnEditView.as_view(),
        name='edit_column'),
    url(r'^columns/column/update/$', views.ColumnEditView.as_view(),
        name='update_column'),
    url(r'^columns/attributes/update/$', views.SetColumnsAttributesView.as_view(),
        name='update_attributes'),
    url(r'^notify_deadlines/(?P<pk>\d+)/$', views.SendNotificationsView.as_view(), name='send_notifications'),
    url(r'^columns/(?P<column_pk>\d+)/rules/(?P<rule_pk>\d+)/delete$', views.DeleteColumnRuleView.as_view(), name='delete_column_rule'),
    url(r'^columns/(?P<column_id>\d+)/rules/create', views.ColumnRuleCreateView.as_view(), name='create_column_rule'),
)