from django.conf.urls import patterns, include, url
from django.contrib import admin
from dajaxice.core import dajaxice_autodiscover, dajaxice_config

from supervision.views import LoginView, LogoutView, HomeView
import settings

admin.autodiscover()
dajaxice_autodiscover()

urlpatterns = patterns('',
    url(r'^', include('basic.urls', namespace='basic')),
    url(r'^$', HomeView.as_view(), name='first_page'),
    url(r'^boards/', include('boards.urls', namespace='boards')),
    url(r'^supervision/', include('supervision.urls', namespace='supervision')),
    url(r'^accounts/login/$', LoginView.as_view(), name='login'),
    url(r'^accounts/logout/$', LogoutView.as_view(), name='logout'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^analytics/', include('analytics.urls', namespace='analytics')),
    url(r'^%s/' % settings.DAJAXICE_MEDIA_PREFIX, include('dajaxice.urls')),
    url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),
)