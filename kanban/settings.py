"""
Django settings for kanban project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'sd0@cw@14b&a$7l_th72o+x10lq!(tcbzct&l5726kbvx$7h1_'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []
DAJAXICE_MEDIA_PREFIX = "s13/dajaxice"

# Application definition
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'lockout',
    'basic',
    'boards',
    'supervision',
    'analytics',
    'dajaxice',
    'dajax',
    'jquery',
)

MIDDLEWARE_CLASSES = (
    'lockout.middleware.LockoutMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'kanban.urls'

WSGI_APPLICATION = 'kanban.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        #'ENGINE': 'django.db.backends.mysql',
        #'NAME': 's13_2014',
        #'USER': 's13al',
        #'PASSWORD': '628ati59',
        #'HOST': 'ltpo2.fri1.uni-lj.si',
        #'PORT': '3306',
    }
}

AUTH_USER_MODEL = 'boards.User'

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'dajaxice.finders.DajaxiceFinder',
)

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'kanban.notice@gmail.com'
EMAIL_HOST_PASSWORD = 'smrpotpo13'
EMAIL_PORT = 587
LOCKOUT_MAX_ATTEMPTS = 3  # How many attempts does the use have before he is locked out.
LOCKOUT_TIME = 10 * 60    # How many seconds is the users locked out for.

from apscheduler.scheduler import Scheduler
from boards.management.tasks.generate_notice import check_all_boards
sched = Scheduler()
sched.start()
sched.add_interval_job(check_all_boards, hours=24)

