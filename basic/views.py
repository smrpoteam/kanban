from django.shortcuts import render
from django.views.generic import TemplateView
from braces.views import LoginRequiredMixin

# Create your views here.


class DocumentationTemplateView(LoginRequiredMixin, TemplateView):
    template_name = 'basic/documentation/main.html'