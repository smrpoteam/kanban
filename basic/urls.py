from django.conf.urls import patterns, include, url
from django.contrib import admin

import views

admin.autodiscover()


urlpatterns = patterns('',
    url(r'^documentation/$', views.DocumentationTemplateView.as_view(), name='documentation'),
)
