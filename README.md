Coding standards:
- Editor width must be set to 100 chars.
- Obey PEP8!


Loading fixtures:
- run: python manage.py loaddata init_data.json
- to edit fixtures: boards/fixtures/init_data.json

Default data:
- Username is not part of User model any more. Instead an unique email has to be provided.
- all users have password 'test'
- users in fixtures:
    * admin@kanban.com          -> group 'Administrator'
    * kanban_master@kanban.com  -> group 'Kanban master'
    * product_owner@kanban.com  -> group 'Product owner'
    * developer_1@kanban.com    -> group 'Developer'
    * developer_2@kanban.com    -> group 'Developer'

Delete data:
- in order to delete data run: python manage.py delete_data

If someone misses jquery imports, download and put them in the static folder
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">

Do not change existing fixtures if they are not yours. Generate new testing examples.