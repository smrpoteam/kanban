$(document).ready(function () {
    $('input').addClass('form-control');
    $('select').addClass('form-control');

    Dajaxice.supervision.get_all_users(setUpUserSet);

    $('.add-user').click(function(ev){
        ev.preventDefault();

        var count = parseFloat($('#id_userteam_set-TOTAL_FORMS').attr('value'));
        var tmplMarkup = $('#member-template').html();
        var compiledTmpl = _.template(tmplMarkup, { id : count });
        $('#id_userteam_set-TOTAL_FORMS').attr('value', count + 1);

        $('table.member-container').find('tbody').append(compiledTmpl);

        $('table.member-container').find('tbody').find('tr').last()
            .find('select').first().html(($('#user-set').html()));
      });

    $(document).on('click', '.user select', function () {
        var num = $(this).closest('tr').attr('id').split('-')[1];
        Dajaxice.supervision.get_user_roles(addMember, { 'user_id':$(this).val(), 'number': num});
        return
    });

    function addMember(data){
        var parsed = JSON.parse(data.roles);
        var select = $('#id_userteam_set-' + data.number + '-group');

        select.html("");
        select.append('<option> --------- </option>');
        parsed.forEach(function(item){
            var e = '<option value=' + item.pk + '>' + item.fields.name + '</option>';
            select.append(e);
        });
    }

    function setUpUserSet(data){
        var parsed = JSON.parse(data);
        var select = $('#user-set')
        select.html("");
        select.append('<option> --------- </option>');

        parsed.forEach(function(item){
            e = '<option value=' + item.pk + '>' + item.fields.first_name + ' ' + item.fields.last_name + '</option>';
            select.append(e);
        });
    }
});