$(document).ready(function() {
    var dateFormat = {'format': 'dd. mm.yyyy'};

    $('#id_start_date').datepicker(dateFormat);
    $('#id_end_date').datepicker(dateFormat);
});
