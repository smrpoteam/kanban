from django import forms


class MessageCharField(forms.CharField):
    """Textarea for messages."""

    def __init__(self, max_length=None, widget=None, attrs=None, *args, **kwargs):
        max_length = max_length or 500
        widget = widget or forms.Textarea({'rows': '3'})
        if attrs:
            widget.attrs.update(attrs)

        super(MessageCharField, self).__init__(max_length=max_length, widget=widget,
                                               *args, **kwargs)
