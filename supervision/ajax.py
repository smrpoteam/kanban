from dajaxice.decorators import dajaxice_register
from django.core import serializers
import json

from boards.models import Project, User, Team


@dajaxice_register
def get_projects(request, team_pk):
    projects = [p for p in Project.objects.all() if
                p not in Team.objects.filter(pk=team_pk).first().projects.all()]
    serialized_projects = serializers.serialize('json', projects,
                                                fields={'code', 'name'})

    return json.dumps(serialized_projects)


@dajaxice_register
def get_users(request, team_pk):
    users = [u for u in User.objects.all() if
             u not in Team.objects.filter(pk=team_pk).first().users.all()]
    serialized_users = serializers.serialize('json', users,
                                             fields=('pk', 'email', 'first_name', 'last_name'))

    return json.dumps(serialized_users)


@dajaxice_register
def get_user_roles(request, user_id, number):
    user_roles = User.objects.filter(pk=user_id).first().groups.all()
    serialized_roles = serializers.serialize('json', user_roles)

    return json.dumps({'roles': serialized_roles, 'number': number})


@dajaxice_register
def get_all_users(request):
    users = serializers.serialize('json', User.objects.filter(is_active=True))
    return json.dumps(users)