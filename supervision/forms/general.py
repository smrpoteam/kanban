from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login
from django.forms.forms import NON_FIELD_ERRORS
from django.core.exceptions import ValidationError

from lockout import LockedOut

class LoginForm(AuthenticationForm):
    """
    Validates username and password. Performs authentication for provided login credentials. Also
    provides method for login and locks out users, if he has to many login attempts.
    """

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if not (username and password):
            raise forms.ValidationError('Please enter username and password.')

        try:
            self.user_cache = authenticate(username=username, password=password)
        except LockedOut:
            raise ValidationError('Login is temporarily disabled due to high amount of '
                                  'unsuccessful attempts from your address. Try again later')
        if self.user_cache is None:
            raise forms.ValidationError(
                self.error_messages['invalid_login'],
                code='invalid_login',
                params={'username': self.username_field.verbose_name},
            )
        elif not self.user_cache.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

        return self.cleaned_data

    def login_user(self, request):
        """
        When login is performed, it also increases login counter.
        """

        user = self.get_user()
        if not user:
            raise Exception('No users found, so no login action can be performed.')
        login(request, user)

    @property
    def formatted_error(self):
        if NON_FIELD_ERRORS in self.errors:
            return self.errors[NON_FIELD_ERRORS][0]
        return 'Please enter a correct username and password.'