from django import forms
from django.core.exceptions import ValidationError
from django.forms.models import inlineformset_factory
from django.forms.models import BaseInlineFormSet

from datetime import datetime

from ..views.base import SetInactiveMixin
from boards.models import *


class UserTeamFormSet(BaseInlineFormSet):
    def clean(self):
        if any(self._errors):
            super(UserTeamFormSet, self).clean()
            return

        cleaned_data = [c_data for c_data in self.cleaned_data if c_data and c_data['is_active']]

        for form in cleaned_data:
            if sum(1 for u in cleaned_data if u['user'].pk == form['user'].pk and
                            u['group'].pk == form['group'].pk) > 1:
                raise ValidationError('Some rows are duplicated.')

        required_groups = [GROUP_DEVELOPER.lower(),
                           GROUP_KANBAN_MASTER.lower(),
                           GROUP_PRODUCT_OWNER.lower()]

        groups = [ut['group'].name.lower() for ut in cleaned_data]
        if not sum(1 for group in required_groups if group in groups) == len(required_groups):
            raise ValidationError('Not all required roles included. Required roles: %(roles)s.',
                                  params={'roles': required_groups},
                                  code='required roles not supported')

        for unique_group in [GROUP_KANBAN_MASTER.lower(), GROUP_PRODUCT_OWNER.lower()]:
            if sum(1 for group in groups if group == unique_group) > 1:
                raise ValidationError('A team can only have one %(group)s.',
                                      params={'group': unique_group})

        super(UserTeamFormSet, self).clean()


class TeamForm(SetInactiveMixin, forms.ModelForm):
    def save(self):
        team = self.instance
        team.save()

        return team

    class Meta:
        model = Team


class TeamCreateForm(TeamForm):
    class Meta:
        model = Team
        fields = ('name',)

UserTeamSet = inlineformset_factory(Team, UserTeam, max_num=3, formset=UserTeamFormSet)