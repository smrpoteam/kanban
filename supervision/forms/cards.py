from django import forms
from django.db.models import Q
from .base import StyledModelForm
from ..fields import MessageCharField
from boards.config import DATE_FORMAT, GROUP_DEVELOPER
from boards.models import User, Card, CardHistory, Column, MoveColumnRule


class WipViolationMixin(object):
    """Mixin, which contains method for checking WIP violation and saving the card history."""

    def _check_wip_violation(self, column, wip_cause):
        """
        Checks if there is WIP violation for specified columns and all it's parents. It requires,
        that form has field wip_cause.
        """

        while column:
            card_count = column.card_count

            # If WIP is 0, then column can have unlimited number of cards.
            wip_violation = column.wip and card_count >= column.wip
            if wip_violation and not wip_cause:
                error_message = "Max WIP reached for column '%(column)s'. Current WIP counter: " \
                                "%(wip_counter)d, max WIP: %(max_wip)d. Please enter cause if " \
                                "you want to continue." % {
                                    'column': column.name,
                                    'wip_counter': card_count + 1,
                                    'max_wip': column.wip,
                                }
                errors = self.error_class([error_message])
                self._errors.setdefault('wip_cause', []).extend(errors)
                return column

            column = column.parent_column

    def _save_card_history(self, card, history_dict):
        """
        For provided card and default data in history dict saves history. Form must have fields
        wip_cause, deletion_cause or both.
        """

        if self.cleaned_data.get('wip_cause'):
            history_dict['description'] = self.cleaned_data['wip_cause']
            history_dict['action'] = CardHistory.ACTION_CARD_WIP_VIOLATION

        card.cardhistory_set.create(**history_dict)


class CardBaseModelForm(StyledModelForm):
    def __init__(self, user, project, *args, **kwargs):
        self._user, self._project = user, project

        super(CardBaseModelForm, self).__init__(*args, **kwargs)

        self.fields['deadline'].widget.format = DATE_FORMAT
        self.fields['deadline'].input_formats = [DATE_FORMAT]
        self.fields['description'].widget = forms.Textarea(attrs={
            'class': 'form-control', 'rows': '3',
        })

        users = User.objects.filter(is_active=True).filter(team__project=self._project).distinct()
        self.fields['assignee'].queryset = users.filter(userteam__group__name=GROUP_DEVELOPER)

        self.fields['size'].initial = ''
        # Product owner can only create regular card and kanban master can only crete SB card.
        if self._user.is_product_owner(self._project):
            self.fields['color'].initial = Card.COLOR_REGULAR
        else:
            self.fields['color'].initial = Card.COLOR_SILVER_BULLET

    class Meta:
        model = Card


class CardCreateModelForm(WipViolationMixin, CardBaseModelForm):
    # Description entered by user, when he makes WIP violation.
    wip_cause = MessageCharField(required=False, attrs={'placeholder': 'WIP violation cause.'})

    def __init__(self, *args, **kwargs):
        super(CardCreateModelForm, self).__init__(*args, **kwargs)
        if self._user.is_kanban_master(self._project):
            self.fields['priority'].choices = [(Card.PRIORITY_MUST_HAVE, 'Must Have')]

    def clean(self):
        """In case of WIP violation user has to provide a cause for WIP violation."""

        cleaned_data = super(CardCreateModelForm, self).clean()
        if self._errors:
            return cleaned_data

        wip_cause = cleaned_data.get('wip_cause')
        self._set_instance_data()
        self._check_wip_violation(self.instance.column, wip_cause)

        return cleaned_data

    def save(self, commit=True):
        """Alongside card also saves card history."""

        card = super(CardBaseModelForm, self).save(commit=commit)
        history_dict = {'action': CardHistory.ACTION_CARD_CREATED, 'card': card,
                        'column_from': None, 'column_to': card.column, 'user': self._user}
        self._save_card_history(card, history_dict)

        return card

    def _set_instance_data(self):
        """Sets data on instance according to user's role."""

        self.instance.project = self._project

        if self._user.is_product_owner(self._project):
            self.instance.column = self._project.board.first_column
            self.instance.card_type = Card.TYPE_REGULAR
        elif self._user.is_kanban_master(self._project):
            self.instance.column = self._project.board.priority_column
            self.instance.card_type = Card.TYPE_SILVER_BULLET

    class Meta(CardBaseModelForm.Meta):
        fields = ('name', 'priority', 'deadline', 'assignee', 'size', 'color', 'description',
                  'wip_cause')


class CardUpdateModelForm(CardBaseModelForm):
    def __init__(self, user, project, card, *args, **kwargs):
        super(CardUpdateModelForm, self).__init__(user, project, *args, **kwargs)

        # Priority and card type can only be updated, if card is in pre development columns
        pre_development_columns = self.instance.project.board.pre_development_columns
        is_pre_development = self.instance.column in pre_development_columns

        self.can_update_priority = is_pre_development
        self.fields['priority'].required = self.can_update_priority
        self.can_update_type = is_pre_development and not user.is_developer(project)
        self.fields['card_type'].required = self.can_update_type

        # Developer can't update card type. Only kanban master can update card to silver bullet.
        self.fields['card_type'].choices = [(Card.TYPE_REGULAR, 'Regular')]
        if user.is_kanban_master(project) or card.card_type == Card.TYPE_SILVER_BULLET:
            self.fields['card_type'].choices.append((Card.TYPE_SILVER_BULLET, 'Silver Bullet'))

    def _post_clean(self):
        if not self.can_update_priority:
            self.cleaned_data['priority'] = self.instance.priority
        if not self.can_update_type:
            self.cleaned_data['card_type'] = self.instance.card_type
        super(CardUpdateModelForm, self)._post_clean()

    class Meta(CardBaseModelForm.Meta):
        fields = ['name', 'priority', 'deadline', 'assignee', 'size', 'color', 'description',
                  'card_type']


class CardDeleteModelForm(forms.ModelForm):
    # Description entered by user, when he deletes card.
    deletion_cause = MessageCharField()

    def __init__(self, user, *args, **kwargs):
        self._user = user
        super(CardDeleteModelForm, self).__init__(*args, **kwargs)

    def _post_clean(self):
        super(CardDeleteModelForm, self)._post_clean()
        self.instance.is_active = False

    class Meta:
        model = Card
        fields = ('deletion_cause',)

    def save(self, commit=True):
        card = super(CardDeleteModelForm, self).save(commit=commit)
        card.cardhistory_set.create(
            action=CardHistory.ACTION_CARD_DELETED, card=card, user=self._user,
            description=self.cleaned_data['deletion_cause']
        )
        return card


class MoveCardModelForm(WipViolationMixin, StyledModelForm):
    # Description entered by user, when he makes WIP violation.
    wip_cause = MessageCharField(required=False, attrs={'placeholder': 'WIP violation cause.'})
    moving_rule_violation = MessageCharField(required=False,
                                             attrs={'placeholder': 'Card moving rule violation.'})

    def __init__(self, user, project, card, *args, **kwargs):
        """Specified column's query set according to properties of user, project and card."""

        self._user = user
        self._project = project
        super(MoveCardModelForm, self).__init__(*args, **kwargs)

        column, board, columns = self.instance.column, self._project.board, []
        next_column, previous_column = column.next_active_column, column.previous_active_column
        priority_column = board.priority_column
        acceptance_column = board.acceptance_column
        is_silver_bullet = card.card_type == Card.TYPE_SILVER_BULLET

        # There can be only one silver bullet card in priority column.
        if not is_silver_bullet or next_column != priority_column or not \
                next_column.silver_bullet_cards.exists():
            columns.append(next_column)

        # In post development columns card cannot be moved to left. Card cannot be moved from
        # development to pre development columns. Priority column can only have one SB card.
        if column not in board.post_development_columns and column != board.start_boundary_column \
                and (not is_silver_bullet or previous_column != priority_column or
                     not previous_column.silver_bullet_cards.exists()):
            columns.append(previous_column)

        # Product owner can move card from acceptance column to priority column and left to it.
        pre_priority_columns = priority_column.previous_active_columns
        self._pre_priority_columns = pre_priority_columns + [priority_column]
        if self._user.is_product_owner(self._project) and column.is_acceptance:
            columns.extend(self._pre_priority_columns)

        user_team = project.team.userteam_set.filter(user=user).first()
        move_rules = MoveColumnRule.objects.filter(user_group=user_team.group, is_active=True)
        move_rules = move_rules.filter(
            Q(column_origin=column) | Q(column_origin=column.parent_most_column)
        )
        if move_rules:
            for target in (rule.column_target for rule in move_rules):
                children = target.get_all_children
                columns.extend(children) if children else columns.append(target)

        # Card must have all tasks completed in order to move to column acceptance ready.
        if not card.is_done and acceptance_column in columns:
            columns = [c for c in columns if c != acceptance_column]

        columns = Column.objects.filter(pk__in=[c.pk for c in columns if c])
        self.fields['column'].queryset = columns

    def clean(self):
        """Checks if card is being moved to valid column and for WIP violation."""

        cleaned_data = super(MoveCardModelForm, self).clean()
        if self._errors:
            return cleaned_data

        column_from = self.cleaned_data['column_from'] = self.instance.column
        column_to = self.cleaned_data.get('column')
        column_to_parent = column_to.parent_most_column
        column_from_parent = column_from.parent_most_column

        # If card is moved between columns, which have same parent, than this is not WIP violation.
        # Exception is, when target column has WIP, which is lower than parent's WIP.
        violated_column = self._check_wip_violation(column_to, cleaned_data.get('wip_cause'))
        if column_to_parent == column_from_parent == violated_column:
            del self._errors['wip_cause']

        if column_from.is_acceptance and column_to and column_to in self._pre_priority_columns:
            self.instance.color = Card.COLOR_REJECTED

        return cleaned_data

    def save(self, commit=True):
        """Alongside card also saves card history."""

        card = super(MoveCardModelForm, self).save(commit=commit)
        history_dict = {'action': CardHistory.ACTION_CARD_MOVED, 'card': card, 'user': self._user,
                        'column_from': self.cleaned_data['column_from'], 'column_to': card.column}
        self._save_card_history(card, history_dict)

        return card

    class Meta:
        model = Card
        fields = ('column', 'wip_cause', 'moving_rule_violation')
