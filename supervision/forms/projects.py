from .base import StyledModelForm
from boards.config import DATE_FORMAT
from boards.models import Project, Team


class ProjectCreateForm(StyledModelForm):
    def __init__(self, user=None, *args, **kwargs):
        super(ProjectCreateForm, self).__init__(*args, **kwargs)

        self.fields['start_date'].widget.format = DATE_FORMAT
        self.fields['end_date'].widget.format = DATE_FORMAT
        self.fields['start_date'].input_formats = [DATE_FORMAT]
        self.fields['end_date'].input_formats = [DATE_FORMAT]

        self.fields['board'].queryset = user.boards_active
        self.fields['team'].queryset = Team.objects.filter(is_active=True)

    class Meta:
        model = Project


class ProjectUpdateForm(ProjectCreateForm):
    def __init__(self, *args, **kwargs):
        super(ProjectUpdateForm, self).__init__(*args, **kwargs)
        if self.instance.has_cards:
            self.fields['start_date'].widget.attrs['disabled'] = 'disabled'
            self.fields['is_active'].widget.attrs['disabled'] = 'disabled'
