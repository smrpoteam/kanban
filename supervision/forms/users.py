from django import forms
from django.utils.translation import ugettext_lazy as _

from .base import StyledModelForm
from boards.validators import PASSWORD_MIN_LEN, PasswordRegexValidator
from boards.models import User


class UserCreateForm(StyledModelForm):
    password_1 = forms.CharField(min_length=PASSWORD_MIN_LEN, widget=forms.PasswordInput,
                                 validators=[PasswordRegexValidator()])
    password_2 = forms.CharField(min_length=PASSWORD_MIN_LEN, widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super(UserCreateForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['groups'].required = True

    def clean_password_2(self):
        password_1 = self.cleaned_data.get("password_1")
        password_2 = self.cleaned_data.get("password_2")

        if password_1 and password_2 and password_1 != password_2:
            raise forms.ValidationError(_("The two password fields didn't match."),
                                        code='password_mismatch',)
        return password_2

    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)
        if self.cleaned_data['password_1']:
            user.set_password(self.cleaned_data["password_1"])

        if commit:
            user.save()
            self.save_m2m()
        return user

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'is_active', 'password_1', 'password_2',
                  'groups',)


class UserUpdateForm(UserCreateForm):
    def __init__(self, *args, **kwargs):
        super(UserUpdateForm, self).__init__(*args, **kwargs)
        self.fields['password_1'].required = False
        self.fields['password_2'].required = False
