from django import forms
from django.core.exceptions import ValidationError
from django.http.request import QueryDict


class CustomFormLogic(object):
    """
    Form strips all string fields, which are listed in Meta class field strip_fields. By default all
    string fields are stripped.
    Form also has method to update form data dict, even if it is immutable.
    It contains method to add custom style to form fields.
    """

    def _clean_fields(self):
        for name, field in self.fields.items():
            # value_from_datadict() gets the data from the data dictionaries.
            # Each widget type knows how to retrieve its own data, because some
            # widgets split data over several HTML fields.
            value = field.widget.value_from_datadict(self.data, self.files, self.add_prefix(name))
            if (not hasattr(self.Meta, 'strip_fields') or name in self.Meta.strip_fields) and \
                    isinstance(value, (str, unicode)):
                value = value.strip()
                self._update_data(name, value)

            try:
                if isinstance(field, forms.FileField):
                    initial = self.initial.get(name, field.initial)
                    value = field.clean(value, initial)
                else:
                    value = field.clean(value)
                self.cleaned_data[name] = value
                if hasattr(self, 'clean_%s' % name):
                    value = getattr(self, 'clean_%s' % name)()
                    self.cleaned_data[name] = value
            except ValidationError as e:
                self._errors[name] = self.error_class(e.messages)
                if name in self.cleaned_data:
                    del self.cleaned_data[name]

    def _update_data(self, field_name, value):
        if isinstance(self.data, QueryDict):
            self.data._mutable = True
        self.data[field_name] = value
        if isinstance(self.data, QueryDict):
            self.data._mutable = False

    def __init__(self, *args, **kwargs):
        super(CustomFormLogic, self).__init__(*args, **kwargs)
        self._add_style_to_fields()

    def _add_style_to_fields(self):
        pass


class CustomForm(CustomFormLogic, forms.Form):
    pass


class CustomModelForm(CustomFormLogic, forms.ModelForm):
    pass


class StyledModelForm(CustomModelForm):
    def _add_style_to_fields(self):
        for field_name, field in self.fields.iteritems():
            if not isinstance(field, forms.BooleanField):
                self.fields[field_name].widget.attrs['class'] = 'form-control'
