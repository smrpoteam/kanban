from django.conf.urls import patterns, url

import views


urlpatterns = patterns('',
    url(r'^$', views.HomeView.as_view(), name='home'),
)

# <editor-fold desc="User view urls">
urlpatterns += patterns('',
    url(r'^users/$', views.UserListView.as_view(), name='user_list'),
    url(r'^users/active/(?P<is_active>(True|False))/$', views.UserListView.as_view(),
        name='user_list'),
    url(r'^users/(?P<pk>\d+)/$', views.UserDetailView.as_view(), name='user_detail'),
    url(r'^users/create/$', views.UserCreateView.as_view(), name='user_create'),
    url(r'^users/(?P<pk>\d+)/update/$', views.UserUpdateView.as_view(), name='user_update'),
    url(r'^users/(?P<pk>\d+)/delete/$', views.UserDeleteView.as_view(), name='user_delete'),
)
# </editor-fold>

# <editor-fold desc="Project view urls">
urlpatterns += patterns('',
    url(r'^projects/$', views.ProjectListView.as_view(), name='project_list'),
    url(r'^projects/active/(?P<is_active>(True|False))/$', views.ProjectListView.as_view(),
        name='project_list'),
    url(r'^projects/(?P<pk>\d+)/$', views.ProjectDetailView.as_view(), name='project_detail'),
    url(r'^projects/create/$', views.ProjectCreateView.as_view(), name='project_create'),
    url(r'^projects/(?P<pk>\d+)/update/$', views.ProjectUpdateView.as_view(),
        name='project_update'),
)
# </editor-fold>

# <editor-fold desc="Card view urls">
urlpatterns += patterns('',
    url(r'^cards/$', views.CardListView.as_view(), name='card_list'),
    url(r'^cards/active/(?P<is_active>(True|False))/$', views.CardListView.as_view(),
        name='card_list'),
    url(r'^cards/(?P<pk>\d+)/$', views.CardDetailView.as_view(), name='card_detail'),
    url(r'^cards/create/$', views.CardCreateView.as_view(), name='card_create'),
    url(r'^cards/(?P<pk>\d+)/update/$', views.CardUpdateView.as_view(), name='card_update'),
    url(r'^cards/(?P<pk>\d+)/delete/$', views.DeleteCardUpdateView.as_view(), name='card_delete'),
    url(r'^cards/move/(?P<pk>\d+)/$', views.MoveCardUpdateView.as_view(), name='card_move'),
)
# </editor-fold>

# <editor-fold desc="Team view urls">
urlpatterns += patterns('',
    url(r'^teams/$', views.TeamListView.as_view(), name='team_list'),
    url(r'^teams/active/(?P<is_active>(True|False))/$', views.TeamListView.as_view(),
        name='team_list'),
    url(r'^teams/(?P<pk>\d+)/$', views.TeamDetailView.as_view(), name='team_detail'),
    url(r'^teams/create/$', views.TeamCreateView.as_view(), name='team_create'),
    url(r'^teams/(?P<pk>\d+)/update/$', views.TeamUpdateView.as_view(), name='team_update'),
)
# </editor-fold>