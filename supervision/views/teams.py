from django.shortcuts import render, redirect
from django.views.generic import View, DetailView, ListView
from django.core.urlresolvers import reverse

from braces.views import LoginRequiredMixin
from braces.views._access import GroupRequiredMixin

from .base import SetInactiveMixin, FilterActiveMixin
from boards.models import Team, User, GROUP_KANBAN_MASTER
from ..forms import TeamForm, UserTeamSet, TeamCreateForm


class UserKanbanMasterMixin(LoginRequiredMixin, GroupRequiredMixin):
    model = User
    group_required = GROUP_KANBAN_MASTER


class TeamListView(LoginRequiredMixin, FilterActiveMixin, ListView):
    model = Team
    context_object_name = 'teams'
    template_name = 'supervision/teams/list.html'


class TeamDetailView(LoginRequiredMixin, DetailView):
    model = Team
    template_name = 'supervision/teams/detail.html'


class TeamCreateView(UserKanbanMasterMixin, SetInactiveMixin, View):
    template_name = 'supervision/teams/create.html'
    form_class = TeamForm

    def get(self, request):
        form = TeamCreateForm()
        formset = UserTeamSet()

        return render(request, 'supervision/teams/create.html', {
            'form': form,
            'formset': formset
        })

    def post(self, request):
        form = TeamCreateForm(request.POST)
        formset = UserTeamSet(request.POST, instance=form.instance)

        if form.is_valid() and formset.is_valid():
            team = form.save()
            formset.save()

            return redirect(reverse('supervision:team_detail', args=(team.pk,)))

        return render(request, 'supervision/teams/create.html', {
            'form': form,
            'formset': formset
        })


class TeamUpdateView(UserKanbanMasterMixin, SetInactiveMixin, View):
    def get(self, request, pk):
        team = Team.objects.filter(pk=pk).first()
        form = TeamForm(instance=team)
        formset = UserTeamSet(instance=team, queryset=team.userteam_set.filter(is_active=True))

        return render(request, 'supervision/teams/update.html', {
            'form': form,
            'formset': formset
        })

    def post(self, request, pk):
        team = Team.objects.filter(pk=pk).first()
        form = TeamForm(request.POST, instance=team)
        formset = UserTeamSet(request.POST, instance=team)

        if formset.is_valid() and form.is_valid():
            formset.save()
            form.save()

            return redirect(reverse('supervision:team_detail', args=(pk,)))

        return render(request, 'supervision/teams/update.html', {
            'form': form,
            'formset': formset
        })