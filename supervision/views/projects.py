from django.views.generic import DetailView, ListView, CreateView, UpdateView

from braces.views import LoginRequiredMixin, GroupRequiredMixin

from .base import FilterActiveMixin, UserToFormKwargsMixin
from ..forms import ProjectCreateForm, ProjectUpdateForm
from boards.config import GROUP_KANBAN_MASTER
from boards.models import Project


class ProjectAdminMixin(LoginRequiredMixin, GroupRequiredMixin):
    model = Project
    group_required = GROUP_KANBAN_MASTER


class ProjectDetailView(ProjectAdminMixin, DetailView):
    template_name = 'supervision/projects/detail.html'


class ProjectListView(ProjectAdminMixin, FilterActiveMixin, ListView):
    context_object_name = 'projects'
    template_name = 'supervision/projects/list.html'


class ProjectCreateView(ProjectAdminMixin, UserToFormKwargsMixin, CreateView):
    form_class = ProjectCreateForm
    template_name = 'supervision/projects/create.html'


class ProjectUpdateView(ProjectAdminMixin, UserToFormKwargsMixin, UpdateView):
    form_class = ProjectUpdateForm
    template_name = 'supervision/projects/update.html'

    def get_form_kwargs(self):
        kwargs = super(ProjectUpdateView, self).get_form_kwargs()
        if 'data' in kwargs and self.object.has_cards:
            kwargs['data']._mutable = True
            kwargs['data']['start_date'] = self.object.start_date
            kwargs['data']['is_active'] = self.object.is_active

        return kwargs
