from django.contrib import messages
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth import logout
from django.views.generic import View, TemplateView
from braces.views import LoginRequiredMixin

from supervision.forms.general import LoginForm


class LoginView(View):
    """
    View for login. Request parameter 'next' can contain the next page to render after login.
    """

    def get(self, request):
        if not request.user.is_authenticated():
            return render(request, 'supervision/login.html', {'form': LoginForm()})
        return render(request, 'supervision/home.html')

    def post(self, request):
        if not request.user.is_authenticated():
            form = LoginForm(data=request.POST)
            if form.is_valid():
                form.login_user(request)
            else:
                messages.warning(request, form.formatted_error)

        return redirect(request.REQUEST.get('next') or reverse('supervision:home'))


class LogoutView(View):
    """
    View for logout. Request parameter 'next' can contain the next page to render after logout.
    """

    def get(self, request):
        logout(request)
        return redirect(request.REQUEST.get('next') or reverse('supervision:home'))


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = 'supervision/home.html'


class ErrorView(LoginRequiredMixin, TemplateView):
    template_name = 'supervision/error.html'