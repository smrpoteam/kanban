from django.http.response import HttpResponseRedirect
from django.http import Http404
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect

from braces.views import LoginRequiredMixin

from boards.config import GROUP_ADMINISTRATOR, GROUP_KANBAN_MASTER, GROUP_DEVELOPER, \
    GROUP_PRODUCT_OWNER
from boards.models import Project


class SetInactiveMixin(object):
    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.is_active = False
        self.object.save()
        return HttpResponseRedirect(success_url)


class FilterActiveMixin(object):
    def get_queryset(self):
        queryset = super(FilterActiveMixin, self).get_queryset()
        is_active = self.kwargs.get('is_active')

        if is_active is None:
            return queryset
        is_active = is_active == 'True'
        return queryset.filter(is_active=bool(is_active))


class NextPageMixin(object):
    def get_success_url(self):
        next_url = self.request.REQUEST.get('next')
        if next_url:
            return next_url

        return super(NextPageMixin, self).get_success_url()


class UserToFormKwargsMixin(object):
    def get_form_kwargs(self):
        kwargs = super(UserToFormKwargsMixin, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs


class PermissionsMixin(LoginRequiredMixin):
    """
    Checks, if logged in user is in required groups for provided project. Get project method has to
    be implemented, in order to retrieve project.
    This Mixin has to be the left most mixin in class based view (exception CsrfExemptMixin).
    """

    groups_required = []
    permission_denied_url = None

    def get_permission_denied_url(self):
        if not self.permission_denied_url:
            raise NotImplementedError('permission_denied_url_has_to_be_provided')

    def get_project(self):
        raise NotImplementedError('get_project method not implemented.')

    def get(self, request, *args, **kwargs):
        return self._validate() or super(PermissionsMixin, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self._validate() or super(PermissionsMixin, self).post(request, *args, **kwargs)

    def _validate(self):
        """Encapsulates validation in order not to repeat the code in get and post methods."""

        self._project = self.get_project()
        self._validate_input()

        # Checks if there are any queued messages.
        if self.request._messages._queued_messages:
            return redirect(self.get_permission_denied_url())

    def _validate_input(self):
        """
        If any addition permissions have to be checked, then this should be done in this method.
        """

        user, has_permission = self.request.user, False

        if GROUP_ADMINISTRATOR in self.groups_required:
            has_permission = has_permission or user.is_admin(self._project)
        if GROUP_KANBAN_MASTER in self.groups_required:
            has_permission = has_permission or user.is_kanban_master(self._project)
        if GROUP_PRODUCT_OWNER in self.groups_required:
            has_permission = has_permission or user.is_product_owner(self._project)
        if GROUP_DEVELOPER in self.groups_required:
            has_permission = has_permission or user.is_developer(self._project)

        if not has_permission:
            messages.info(self.request, 'Permission denied.')
