from django.core.urlresolvers import reverse_lazy
from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView

from braces.views import LoginRequiredMixin, GroupRequiredMixin

from .base import SetInactiveMixin, FilterActiveMixin
from ..forms import UserCreateForm, UserUpdateForm
from boards.config import GROUP_ADMINISTRATOR
from boards.models import User


class UserAdminMixin(LoginRequiredMixin, GroupRequiredMixin):
    model = User
    group_required = GROUP_ADMINISTRATOR


class UserDetailView(UserAdminMixin, DetailView):
    template_name = 'supervision/users/detail.html'


class UserListView(UserAdminMixin, FilterActiveMixin, ListView):
    context_object_name = 'users'
    template_name = 'supervision/users/list.html'


class UserCreateView(UserAdminMixin, CreateView):
    form_class = UserCreateForm
    template_name = 'supervision/users/create.html'


class UserUpdateView(UserAdminMixin, UpdateView):
    form_class = UserUpdateForm
    template_name = 'supervision/users/update.html'


class UserDeleteView(UserAdminMixin, SetInactiveMixin, DeleteView):
    template_name = 'supervision/delete.html'
    success_url = reverse_lazy('supervision:user_list')
