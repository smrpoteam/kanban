from django.http.response import Http404
from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.views.generic import DetailView, ListView, CreateView, UpdateView

from .base import PermissionsMixin, FilterActiveMixin, UserToFormKwargsMixin
from ..forms import CardCreateModelForm, CardUpdateModelForm, CardDeleteModelForm, MoveCardModelForm
from boards.config import GROUP_PRODUCT_OWNER, GROUP_KANBAN_MASTER, GROUP_DEVELOPER
from boards.models import Card, Project
from boards.views import ShowBoardView, DEFAULT_CRITICAL_CARD_THRESHOLD


class CardAdminMixin(PermissionsMixin):
    """Checks if user has required permissions."""

    model = Card

    def get_project(self):
        if 'pk' in self.kwargs or 'slug' in self.kwargs:
            self.object = self.get_object()
            return self.object.project
        elif 'project_pk' in self.request.REQUEST:
            return get_object_or_404(Project, pk=self.request.REQUEST['project_pk'])
        else:
            raise Http404('Card or project id has to be provided.')

    def _validate_input(self):
        super(CardAdminMixin, self)._validate_input()

        if not self._project.board.has_columns:
            messages.info(self.request, "Project doesn't have active columns.")

    def get_permission_denied_url(self):
        return reverse('boards:show_board', args=(self._project.board.pk,))


class DataToFormKwargsMixin(UserToFormKwargsMixin):
    """Has to be placed left to ValidateInputMixin in order to have project."""

    def get_form_kwargs(self):
        kwargs = super(DataToFormKwargsMixin, self).get_form_kwargs()
        kwargs['project'] = self._project
        return kwargs


class CardDetailView(CardAdminMixin, DetailView):
    template_name = 'supervision/cards/detail.html'
    groups_required = [GROUP_KANBAN_MASTER, GROUP_PRODUCT_OWNER, GROUP_DEVELOPER]


# Currently not used.
class CardListView(CardAdminMixin, FilterActiveMixin, ListView):
    template_name = 'supervision/cards/list.html'


class CardCreateView(CardAdminMixin, DataToFormKwargsMixin, CreateView):
    form_class = CardCreateModelForm
    template_name = 'supervision/cards/create.html'
    groups_required = [GROUP_KANBAN_MASTER, GROUP_PRODUCT_OWNER]

    def _validate_input(self):
        """Kanban master adds cards to priority column, so it has to exist."""

        super(CardCreateView, self)._validate_input()
        if not self.request.user.is_kanban_master(self._project):
            return

        priority_column = self._project.board.priority_column
        if not priority_column:
            messages.info(self.request, 'Board doesnt have a priority column.')
        if priority_column.silver_bullet_cards.exists():
            messages.info(self.request, 'Priority column can only have 1 silver bullet card.')


class CardUpdateView(CardAdminMixin, DataToFormKwargsMixin, UpdateView):
    form_class = CardUpdateModelForm
    template_name = 'supervision/cards/update.html'
    groups_required = [GROUP_KANBAN_MASTER, GROUP_PRODUCT_OWNER, GROUP_DEVELOPER]

    def _validate_input(self):
        super(CardUpdateView, self)._validate_input()

        self.object = self.get_object()
        if not self.object.can_update_card(self.request.user):
            messages.info(self.request, "You don't have permission to edit card in column "
                                        "%(column)s." % {'column': self.object.column})

    def get_form_kwargs(self):
        kwargs = super(CardUpdateView, self).get_form_kwargs()
        kwargs['card'] = self.object
        return kwargs


class DeleteCardUpdateView(CardAdminMixin, UserToFormKwargsMixin, UpdateView):
    form_class = CardDeleteModelForm
    groups_required = [GROUP_KANBAN_MASTER, GROUP_PRODUCT_OWNER]

    def get(self, request, *args, **kwargs):
        raise Http404()

    def _validate_input(self):
        super(DeleteCardUpdateView, self)._validate_input()
        if not self.object.can_delete_card(self.request.user):
            messages.info(self.request, "You don't have permission to delete card.")
        if not self.object.is_active:
            messages.info(self.request, "Card is already deleted.")


class MoveCardUpdateView(CardAdminMixin, DataToFormKwargsMixin, UpdateView):
    form_class = MoveCardModelForm
    groups_required = [GROUP_KANBAN_MASTER, GROUP_PRODUCT_OWNER, GROUP_DEVELOPER]

    def get(self, request, *args, **kwargs):
        raise Http404()

    def form_invalid(self, form):
        return ShowBoardView().get(self.request, self.object.column.board.pk,
                                   critical_card_threshold=DEFAULT_CRITICAL_CARD_THRESHOLD,
                                   critical_card_error=False, form=form)

    def get_form_kwargs(self):
        kwargs = super(MoveCardUpdateView, self).get_form_kwargs()
        kwargs['card'] = self.object
        return kwargs
