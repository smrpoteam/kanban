import datetime
import time

from django.shortcuts import get_object_or_404
from boards.models import Board, Project, Card, CardHistory, Column


def card_rejected(card, accept_col, pre_dev_cols):
    # card which we want to know if it was rejected in the past
    # accept_col: acceptance column of the board the card is on
    # pre_dev_cols: pre-development columns of the board the card is on
    # return true/false
    # TODO: test this!!!

    card_history = CardHistory.objects.filter(card=card, action=CardHistory.ACTION_CARD_MOVED)
    rejected = card_history.filter(column_from=accept_col, column_to__in=pre_dev_cols)
    if rejected.count() > 0:
        return True
    return False


def get_cards_subset(post, cards, context):
    # check for card types
    board = None
    if post.get('selected_board'):
        board = get_object_or_404(Board, pk=int(post.get('selected_board')))
    card_type = 'all'
    if post.get('type'):
        card_type = str(post['type'])
        if card_type == 'regular':
            context['is_regular'] = True
        elif card_type == 'silver':
            context['is_silver'] = True
        elif card_type == 'rejected':
            context['is_rejected'] = True

    if card_type == 'regular':
        cards = cards.filter(card_type=Card.TYPE_REGULAR)
    elif card_type == 'silver':
        cards = cards.filter(card_type=Card.TYPE_SILVER_BULLET)
    elif card_type == 'rejected':
        # check in card history if card was rejected
        acceptance_col = board.acceptance_column
        pre_development_cols = board.pre_development_columns
        for c in cards:
            if not card_rejected(c, acceptance_col, pre_development_cols):
                cards = cards.exclude(pk=c.id)

    # check by card size
    #size_from = size_to = None
    if post.get('card_size_from'):
        size_from = int(post.get('card_size_from'))
        cards = cards.filter(size__gte=size_from)
        context['size_from'] = size_from
    if post.get('card_size_to'):
        size_to = int(post.get('card_size_to'))
        cards = cards.filter(size__lte=size_to)
        context['size_to'] = size_to

    # TODO: check for all three date intervals
    # check date created interval
    created_from = created_to = None
    if post.get('created_start'):
        created_from = post.get('created_start')
        context['created_from'] = created_from
        created_from = time.strptime(created_from, "%d.%m.%Y")
        created_from = datetime.datetime.fromtimestamp(time.mktime(created_from))
    if post.get('created_end'):
        created_to = post.get('created_end')
        context['created_to'] = created_to
        created_to = time.strptime(created_to, "%d.%m.%Y")
        created_to = datetime.datetime.fromtimestamp(time.mktime(created_to))
        if created_to > datetime.datetime.now():
            created_to = datetime.datetime.now()
    if created_to and created_from:
        if created_to <= created_from:
            context['created_error'] = True
        else:
            cards_ids = CardHistory.objects.filter(action=CardHistory.ACTION_CARD_CREATED,
                                                   time_created__gte=created_from,
                                                   time_created__lte=created_to).values('card')
            cards = cards.filter(pk__in=cards_ids)
    else:
        if created_to:
            cards_ids = CardHistory.objects.filter(action=CardHistory.ACTION_CARD_CREATED,
                                                   time_created__lte=created_to).values('card')
            cards = cards.filter(pk__in=cards_ids)
        elif created_from:
            cards_ids = CardHistory.objects.filter(action=CardHistory.ACTION_CARD_CREATED,
                                                   time_created__gte=created_from).values('card')
            cards = cards.filter(pk__in=cards_ids)

    # check date done interval
    done_from = done_to = None
    if post.get('done_start'):
        done_from = post.get('done_start')
        context['done_from'] = done_from
        done_from = time.strptime(done_from, "%d.%m.%Y")
        done_from = datetime.datetime.fromtimestamp(time.mktime(done_from))
    if post.get('done_end'):
        done_to = post.get('done_end')
        context['done_to'] = done_to
        done_to = time.strptime(done_to, "%d.%m.%Y")
        done_to = datetime.datetime.fromtimestamp(time.mktime(done_to))
        if done_to > datetime.datetime.now():
            done_to = datetime.datetime.now()

    columns_done = None
    if board:
        columns_done = board.post_acceptance_columns
        acceptance_col = board.acceptance_column
    if done_to and done_from:
        if done_to <= done_from:
            context['done_error'] = True
        else:
            cards_ids = CardHistory.objects.filter(action=CardHistory.ACTION_CARD_MOVED,
                                                   time_created__gte=done_from,
                                                   time_created__lte=done_to,
                                                   column_from=acceptance_col,
                                                   column_to__in=columns_done).values('card')
            cards = cards.filter(pk__in=cards_ids)
    else:
        if done_to:
            cards_ids = CardHistory.objects.filter(action=CardHistory.ACTION_CARD_MOVED,
                                                   time_created__lte=done_to,
                                                   column_from=acceptance_col,
                                                   column_to__to=columns_done).values('card')
            cards = cards.filter(pk__in=cards_ids)
        elif done_from:
            cards_ids = CardHistory.objects.filter(action=CardHistory.ACTION_CARD_MOVED,
                                                   time_created__gte=done_from,
                                                   column_from=acceptance_col,
                                                   column_to__in=columns_done).values('card')
            cards = cards.filter(pk__in=cards_ids)

    # check date development interval
    development_from = development_to = None
    if post.get('development_start'):
        development_from = post.get('development_start')
        context['development_from'] = development_from
        development_from = time.strptime(development_from, "%d.%m.%Y")
        development_from = datetime.datetime.fromtimestamp(time.mktime(development_from))
    if post.get('development_end'):
        development_to = post.get('development_end')
        context['development_to'] = development_to
        development_to = time.strptime(development_to, "%d.%m.%Y")
        development_to = datetime.datetime.fromtimestamp(time.mktime(development_to))
        if development_to > datetime.datetime.now():
            development_to = datetime.datetime.now()

    column_development = None
    if board:
        column_development = board.development_columns
        if column_development:
            column_development = column_development[0]
    if development_to and development_from:
        if development_to <= development_from:
            context['development_error'] = True
        else:
            cards_ids = CardHistory.objects.filter(action=CardHistory.ACTION_CARD_MOVED,
                                                   time_created__gte=development_from,
                                                   time_created__lte=development_to,
                                                   column_to=column_development).values('card')
            cards = cards.filter(pk__in=cards_ids)
    else:
        if development_to:
            cards_ids = CardHistory.objects.filter(action=CardHistory.ACTION_CARD_MOVED,
                                                   time_created__lte=development_to,
                                                   column_to=column_development).values('card')
            cards = cards.filter(pk__in=cards_ids)
        elif development_from:
            cards_ids = CardHistory.objects.filter(action=CardHistory.ACTION_CARD_MOVED,
                                                   time_created__gte=development_from,
                                                   column_to=column_development).values('card')
            cards = cards.filter(pk__in=cards_ids)

    return cards, context