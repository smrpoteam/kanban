from django.conf.urls import patterns, include, url
from django.contrib import admin

import views

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', views.ChooseAnalyticsView.as_view(), name='choose'),
    url(r'^cumulative/choose$', views.CumulativeFlowView.as_view(), name='cumulative_choose'),
    url(r'^cumulative/board_selected$', views.CumulativeProjectsView.as_view(),
        name='cumulative_board_selected'),
    url(r'^cumulative/show$', views.CumulativeFlowView.as_view(), name='cumulative_show'),
    url(r'^wip_violations/choose$', views.WipViolationsView.as_view(), name='wip_choose'),
    url(r'^wip_violations/board_selected$', views.WipViolationsProjectsView.as_view(),
        name='wip_board_selected'),
    url(r'^wip_violations/show$', views.WipViolationsView.as_view(), name='wip_violations_show'),
    url(r'^dev_contribution/choose', views.DeveloperContributionView.as_view(), name='developer_contribution_choose'),
    url(r'^dev_contribution/show$', views.DeveloperContributionView.as_view(), name='developer_contribution'),
    url(r'^dev_contribution/board_selected$', views.DeveloperContributionProjectsView.as_view(),
        name='dev_contr_board_selected'),
    url(r'^average_lead_time/show$', views.AverageLeadTimeView.as_view(),
        name='average_lead_time_show'),
    url(r'^average_lead_time/choose$', views.AverageLeadTimeChooseView.as_view(),
        name='average_lead_time_choose'),
    url(r'^average_lead_time/choose_end$', views.AverageLeadTimeChooseEndView.as_view(),
        name='average_lead_time_choose_end'),
    url(r'^average_lead_time/board_selected$', views.AverageLeadTimeSelectView.as_view(),
        name='average_lead_time_selected'),
)
