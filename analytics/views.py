import datetime
import time
import unicodedata
from itertools import chain

from django.views.generic import View
from django.shortcuts import render, get_object_or_404, redirect

from braces.views import LoginRequiredMixin

from boards.models import Board, Project, Card, CardHistory, User, UserTeam
from utils import get_cards_subset


class ChooseBoardMixin(object):
    def _get_boards(self, user):
        boards = Board.objects.filter(is_active=True, author=user)
        return boards

    def post(self, request):
        board_id = request.POST['board']
        context = {}
        boards = self._get_boards(request.user)

        context['boards'] = boards
        if board_id:
            board = get_object_or_404(Board, pk=board_id)
            projects = Project.objects.filter(is_active=True, board_id=board_id)
            context['selected_board'] = board
            context['projects'] = projects

        return render(request, self.template, context)


class AnalyticsBaseMixin(object):
    def get(self, request):
        context = {}
        boards = self._get_boards(request.user)

        context['boards'] = boards
        return render(request, self.template, context)

    def _get_boards(self, user):
        boards = Board.objects.filter(is_active=True, author=user)
        return boards

    def _get_columns_list(self, board):
        columns_list = []
        col = board.first_column
        if col:
            while col:
                columns_list.append(col)
                if col.has_children():
                    children = col.get_all_children
                    for child in children:
                        columns_list.append(child)
                col = col.next_column
        return columns_list

    def _get_col_dict_from_list(self, col_list):
        dict = {}
        for i in range(len(col_list)):
            dict[col_list[i].name] = i
        return dict


class ChooseAnalyticsView(LoginRequiredMixin, View):
    def get(self, request):

        return render(request, 'analytics/analytics.html')


class CumulativeProjectsView(ChooseBoardMixin, LoginRequiredMixin, View):
    """
    Board is chosen and projects need to be filtered
    """

    template = 'analytics/choose_cumulative.html'


class CumulativeFlowView(AnalyticsBaseMixin, LoginRequiredMixin, View):
    template = 'analytics/choose_cumulative.html'

    def post(self, request):
        board_id = request.POST['selected_board']
        #date_from = datetime.datetime(2014, 5, 5)
        date_from = None
        date_to = datetime.datetime.now()
        context = {}
        boards = self._get_boards(request.user)
        context['boards'] = boards
        if request.POST.get('start_date'):
            date_from = request.POST.get('start_date')
            context['date_from'] = date_from
            date_from = time.strptime(date_from, "%d.%m.%Y")
            date_from = datetime.datetime.fromtimestamp(time.mktime(date_from))
        else:
            context['start_date_error'] = True
            board = get_object_or_404(Board, pk=board_id)
            projects = Project.objects.filter(is_active=True, board=board)
            context['selected_board'] = board
            context['projects'] = projects
            return render(request, self.template, context)

        if request.POST.get('end_date'):
            date_to = request.POST.get('end_date')
            context['date_to'] = date_to
            date_to = time.strptime(date_to, "%d.%m.%Y")
            date_to = datetime.datetime.fromtimestamp(time.mktime(date_to))

        if date_to > datetime.datetime.now():
            date_to = datetime.datetime.now()
        date_difference = date_to - date_from + datetime.timedelta(days=1)
        #print date_from, date_to, date_difference
        if date_difference <= datetime.timedelta(days=0):
            context['end_date_error'] = True
            board = get_object_or_404(Board, pk=board_id)
            projects = Project.objects.filter(is_active=True, board=board)
            context['selected_board'] = board
            context['projects'] = projects
            return render(request, self.template, context)

        if board_id:
            board = get_object_or_404(Board, pk=board_id)
            projects = Project.objects.filter(is_active=True, board=board)
            context['selected_board'] = board
            context['projects'] = projects

            # get selected project and make query for cards
            if request.POST['project']:
                project_id = request.POST['project']
                if str(project_id) != 'all':
                    selected_project = get_object_or_404(Project, pk=project_id)
                    context['selected_project'] = selected_project
                    projects = [selected_project]

            #cards = Card.objects.filter(is_active=True, project__in=projects)
            cards = Card.objects.filter(project__in=projects)

            if not cards:
                context['no_cards'] = True
                return render(request, self.template, context)

            # get cards subset - 3 different intervals, size and type
            cards, context = get_cards_subset(request.POST, cards, context)

            # check which columns subset is selected
            # possible: only children columns or all columns
            children_cols_only = True
            if request.POST.get('columns_subset'):
                cols_subset = request.POST.get('columns_subset')
                if str(cols_subset) == 'all':
                    children_cols_only = False
                    context['allcols_selected'] = True

            if children_cols_only:
                columns = self._get_columns_list_children(board)
            else:
                columns = self._get_columns_list(board)

            columns_dict = self._get_col_dict_from_list(columns)  # lookup table for column indexes

            matrix = [[0 for _ in range(date_difference.days)] for _ in columns]

            attributes = [CardHistory.ACTION_CARD_CREATED, CardHistory.ACTION_CARD_MOVED,
                          CardHistory.ACTION_CARD_WIP_VIOLATION]

            for card in cards:
                card_history = CardHistory.objects.filter(card=card, action__in=attributes)
                if card_history.count() > 0:
                    first_history = card_history.filter(time_created__lte=date_from)
                    if first_history:
                        first_history = first_history.first()
                    else:
                        first_history = card_history.last()
                    card_history = card_history.filter(time_created__gte=first_history.time_created)\
                        .reverse()
                    for i in range(card_history.count()):
                        ch = card_history[i]
                        col_i = columns_dict[ch.column_to.name]
                        if i < card_history.count() - 1:
                            ch_next = card_history[i + 1]
                            matrix = self._increment_days(col_i, ch.time_created,
                                                          ch_next.time_created, matrix,
                                                          date_from)
                            if not children_cols_only and ch.column_to.parent_column:
                                parent_col_i = columns_dict[ch.column_to.parent_column.name]
                                matrix = self._increment_days(parent_col_i, ch.time_created,
                                                              ch_next.time_created, matrix,
                                                              date_from)
                        else:
                            date_to = None
                            # check if card was deleted
                            card_history_deleted = CardHistory.objects.filter(card=card,
                                                            action=CardHistory.ACTION_CARD_DELETED)
                            if card_history_deleted.count() > 0:
                                deleted = card_history_deleted.first()
                                if deleted.time_created > ch.time_created:
                                    date_to = deleted.time_created

                            matrix = self._increment_days(col_i, ch.time_created,
                                                          date_to, matrix, date_from)
                            if not children_cols_only and ch.column_to.parent_column:
                                parent_col_i = columns_dict[ch.column_to.parent_column.name]
                                matrix = self._increment_days(parent_col_i, ch.time_created,
                                                              date_to, matrix, date_from)

            """
            print columns_dict
            # print matrix:
            for line in matrix:
                for el in line:
                    print el,
                print ''
            """
            # create matrix to show in google charts on template
            data = [[0 for _ in range(len(matrix)+1)] for _ in range(len(matrix[0])+1)]
            data[0][0] = "Date"
            # modifiy columns order
            for i in range(len(columns)):
                data[0][i+1] = str(columns[len(columns)-1-i].name)
            for i in range(len(data)-1):
                data[i+1][0] = str((date_from + datetime.timedelta(days=i)).date())
            # copy data from matrix in right order
            for i in range(len(data)-1):
                for j in range(len(data[i])-1):
                    data[i+1][j+1] = matrix[len(matrix)-1-j][i]
            """
            # print data matrix:
            for line in data:
                for el in line:
                    print el,
                print ''
            """
            context['graph_data'] = data

            return render(request, self.template, context)
        return render(request, self.template, context)

    def _get_columns_list_children(self, board):
        columns_list = []
        col = board.first_column
        if col:
            while col:
                if col.has_children():
                    children = col.get_all_children
                    for child in children:
                        columns_list.append(child)
                else:
                    columns_list.append(col)
                col = col.next_column
        return columns_list


    def _increment_days(self, col_index, start_date, end_date, m, date0):
        start_i = (start_date.date() - date0.date()).days
        if start_i < 0:
            start_i = 0
        if end_date:
            end_i = (end_date.date() - date0.date()).days
            if end_i > len(m[0]):
                end_i = len(m[0])
        else:
            end_i = len(m[0])

        for i in range(start_i, end_i):
            m[col_index][i] += 1
        return m


class WipViolationsProjectsView(ChooseBoardMixin, LoginRequiredMixin, View):
    """
    Board is chosen and projects need to be filtered
    """

    template = 'analytics/wip_violations.html'


class WipViolationsView(AnalyticsBaseMixin, LoginRequiredMixin, View):
    template = 'analytics/wip_violations.html'

    def post(self, request):
        board_id = request.POST['selected_board']
        #date_from = datetime.datetime(2014, 5, 5)
        date_from = None
        date_to = datetime.datetime.now()
        context = {}
        boards = self._get_boards(request.user)
        context['boards'] = boards
        if request.POST.get('start_date'):
            date_from = request.POST.get('start_date')
            context['date_from'] = date_from
            date_from = time.strptime(date_from, "%d.%m.%Y")
            date_from = datetime.datetime.fromtimestamp(time.mktime(date_from))
        else:
            context['start_date_error'] = True
            board = get_object_or_404(Board, pk=board_id)
            projects = Project.objects.filter(is_active=True, board=board)
            context['selected_board'] = board
            context['projects'] = projects
            return render(request, self.template, context)

        if request.POST.get('end_date'):
            date_to = request.POST.get('end_date')
            context['date_to'] = date_to
            date_to = time.strptime(date_to, "%d.%m.%Y")
            date_to = datetime.datetime.fromtimestamp(time.mktime(date_to))

        if date_to > datetime.datetime.now():
            date_to = datetime.datetime.now()
        date_difference = date_to - date_from + datetime.timedelta(days=1)
        #print date_from, date_to, date_difference
        if date_difference <= datetime.timedelta(days=0):
            context['end_date_error'] = True
            board = get_object_or_404(Board, pk=board_id)
            projects = Project.objects.filter(is_active=True, board=board)
            context['selected_board'] = board
            context['projects'] = projects
            return render(request, self.template, context)

        if board_id:
            board = get_object_or_404(Board, pk=board_id)
            projects = Project.objects.filter(is_active=True, board=board)
            context['selected_board'] = board
            context['projects'] = projects

            # get selected project and make query for cards
            if request.POST['project']:
                project_id = request.POST['project']
                if str(project_id) != 'all':
                    selected_project = get_object_or_404(Project, pk=project_id)
                    context['selected_project'] = selected_project
                    projects = [selected_project]

            #cards = Card.objects.filter(is_active=True, project__in=projects)
            cards = Card.objects.filter(project__in=projects)

            if not cards:
                context['no_cards'] = True
                return render(request, self.template, context)

            # get cards subset - 3 different intervals, size and type
            cards, context = get_cards_subset(request.POST, cards, context)

            attributes = [CardHistory.ACTION_CARD_WIP_VIOLATION,
                          CardHistory.ACTION_COLUMN_WIP_VIOLATION]

            card_history = CardHistory.objects.filter(card__in=cards,
                                                      action=CardHistory.ACTION_CARD_WIP_VIOLATION)

            board_columns = board.column_set.all()
            column_history = CardHistory.objects.filter(card=None,
                                                        column_from__in=board_columns,
                                                    action=CardHistory.ACTION_COLUMN_WIP_VIOLATION)

            if card_history.count() > 0 or column_history.count() > 0:
                columns_list = self._get_columns_list(board)
                cols_dict = self._get_col_dict_from_list(columns_list)
                print "COLUMNS DICT: ", cols_dict
                card_history_list = list(chain(card_history.all(), column_history.all()))
                card_history_sorted = self._sort_cards_by_columns(card_history_list, cols_dict)
                context['card_history'] = card_history_sorted

            else:
                context['no_history'] = True

            return render(request, self.template, context)
        return render(request, self.template, context)


    def _sort_cards_by_columns(self, card_history_list, cols_dict):
        """
        Sort cards by order of the columns on board
        """
        swapped = True
        unsorted = len(card_history_list)
        while swapped:
            swapped = False
            unsorted -= 1
            for i in range(unsorted):
                ch1 = card_history_list[i]
                ch2 = card_history_list[i+1]
                if cols_dict[ch1.column_to.name] > cols_dict[ch2.column_to.name]:
                    # swap elements i and i+1
                    ch_temp = card_history_list[i]
                    card_history_list[i] = card_history_list[i+1]
                    card_history_list[i+1] = ch_temp
                    swapped = True

        return card_history_list


class DeveloperContributionProjectsView(ChooseBoardMixin, LoginRequiredMixin, View):
    template = 'analytics/developer_contribution.html'


class DeveloperContributionView(AnalyticsBaseMixin, LoginRequiredMixin, View):
    template = 'analytics/developer_contribution.html'

    def post(self, request):
        board_id = request.POST['selected_board']
        context = {}
        boards = self._get_boards(request.user)
        context['boards'] = boards

        if board_id:
            board = get_object_or_404(Board, pk=board_id)
            projects = Project.objects.filter(is_active=True, board=board)
            context['selected_board'] = board
            context['projects'] = projects

            # get selected project and make query for cards
            if request.POST['project']:
                project_id = request.POST['project']
                if str(project_id) != 'all':
                    selected_project = get_object_or_404(Project, pk=project_id)
                    context['selected_project'] = selected_project
                    projects = [selected_project]

            #cards = Card.objects.filter(is_active=True, project__in=projects)
            cards = Card.objects.filter(project__in=projects)

            if not cards:
                context['no_cards'] = True
                return render(request, self.template, context)

            cards, context = get_cards_subset(request.POST, cards, context)
            user_teams = [proj.team.userteam_set.all() for proj in projects]
            developers = set(user.user for user_team in user_teams for user in user_team)
            dev_contribution = []

            for developer in developers:
                full_name = developer.get_full_name()
                dev_cards = cards.filter(assignee=developer, is_active=True)

                item = dict()
                item['name'] = full_name
                item['card_count'] = dev_cards.count()
                item['points'] = sum(c.size for c in dev_cards)
                dev_contribution.append(item)

            cards_other = cards.filter(assignee=None)
            item = dict()
            item['name'] = 'Unassigned'
            item['card_count'] = cards_other.count()
            item['points'] = sum(card.size for card in cards_other)
            dev_contribution.append(item)

            context['dev_contribution'] = dev_contribution

            return render(request, self.template, context)


class AverageLeadTimeView(LoginRequiredMixin, View):
    def get(self, request):
        context = {}
        boards = self._get_boards(request.user)

        context['boards'] = boards
        return render(request, 'analytics/average_lead_time.html', context)

    def post(self, request):
        board_id = request.POST['selected_board']
        context = {}
        boards = self._get_boards(request.user)
        context['boards'] = boards

        if board_id:
            board = get_object_or_404(Board, pk=board_id)
            projects = Project.objects.filter(is_active=True, board=board)
            context['selected_board'] = board
            context['projects'] = projects

            # get selected project
            if request.POST['project']:
                project_id = request.POST['project']
                if str(project_id) != 'all':
                    selected_project = get_object_or_404(Project, pk=project_id)
                    context['selected_project'] = selected_project
                    projects = [selected_project]

            # fill start and end column fields
            first_col = board.first_column
            if first_col and first_col.next_active_columns[:-1] is not None:
                context['start_columns'] = [first_col] + first_col.next_active_columns[:-1]

                if request.POST['selected_start_column'] and \
                                request.POST['selected_start_column'] != 'all':
                    selected_start_column = board.column_set.filter(
                        pk=request.POST['selected_start_column']).first()
                    context['selected_start_column'] = selected_start_column

                    # get only columns after start column
                    context['end_columns'] = selected_start_column.next_active_columns[:-1]

                if request.POST['selected_end_column']:
                    selected_end_column = board.column_set.filter(
                        pk=request.POST['selected_end_column']).first()
                    context['selected_end_column'] = selected_end_column

            #cards = Card.objects.filter(is_active=True, project__in=projects)
            cards = Card.objects.filter(project__in=projects)

            if not cards:
                context['no_cards'] = True
                return render(request, 'analytics/average_lead_time.html', context)

            # get cards subset - 3 different intervals, size and type
            cards, context = get_cards_subset(request.POST, cards, context)

            attributes = [CardHistory.ACTION_CARD_CREATED, CardHistory.ACTION_CARD_MOVED,
                          CardHistory.ACTION_CARD_WIP_VIOLATION]

            # select columns
            start_column = request.POST['selected_start_column']
            end_column = request.POST['selected_end_column']

            if start_column == '':
                # select all columns
                columns = [None, board.first_column] + board.first_column.next_active_columns
            else:
                s_col = board.column_set.filter(pk=start_column).first()

                if end_column == '':
                    # select all columns after s_col
                    columns = [s_col.previous_active_column, s_col] + s_col.next_active_columns
                else:
                    e_col = board.column_set.filter(pk=end_column).first()
                    columns = [s_col.previous_active_column, s_col]
                    col = s_col.next_active_column
                    while col:
                        if col == e_col:
                            columns.append(e_col)
                            columns.append(e_col.next_active_column)
                            break
                        columns.append(col)
                        col = col.next_active_column

            graph_data = []
            card_names = []
            for card in cards:
                if card.is_active:
                    card_names.append(unicodedata.normalize('NFKD', card.name)
                                       .encode('ascii', 'ignore'))
                    matrix = [[0 for _ in range(len(columns))] for _ in range(len(columns))]

                    card_history = CardHistory.objects.filter(card=card, action__in=attributes)\
                        .reverse()
                    if card_history:
                        for history in card_history:
                            index_to = 0
                            for col in columns:
                                if history.column_to == col:
                                    try:
                                        index_for = columns.index(history.column_from)
                                        matrix[index_for][index_to] = history.time_created
                                    except ValueError:
                                        pass

                                index_to += 1

                    line = [0 for _ in range(len(columns))]
                    for mat_col in range(len(columns) - 1):

                        value = datetime.timedelta(0)
                        for i in range(len(columns) - 1):
                            if matrix[i + 1][mat_col + 1] != 0 and matrix[i][mat_col] != 0:
                                value += matrix[i + 1][mat_col + 1] - matrix[i][mat_col]

                            elif matrix[i][mat_col] != 0:
                                all_zero = True
                                for element in matrix[i + 1]:
                                    if element != 0:
                                        all_zero = False
                                        value += element - matrix[i][mat_col]
                                if all_zero:
                                    now = datetime.datetime.now(tz=matrix[i][mat_col].tzinfo)
                                    value += now - matrix[i][mat_col]

                            elif matrix[i][mat_col] != 0 and matrix[i + 1][mat_col - 1] != 0:
                                value += matrix[i + 1][mat_col - 1] - matrix[i][mat_col]

                        line[mat_col] = float("%.1f" % (value.total_seconds() / (3600 * 24)))
                    graph_data.append(line)

            header = ["Column names"]
            for col in columns[1:-1]:
                header.append(str(col.name))

            data = [header]
            if graph_data:
                card_names_index = 0
                summ = []
                for line in graph_data:
                    all_zero = True
                    for el in line:
                        if el != 0:
                            all_zero = False
                            break
                    if not all_zero:
                        data.append([card_names[card_names_index]] + line[1:-1])
                        summ.append([card_names[card_names_index], sum(line[1:-1])])

                    card_names_index += 1

                if len(data) > 1:
                    context['graph_data'] = data
                    context['sum'] = summ
                else:
                    context['error'] = True
            else:
                context['error'] = True

            return render(request, 'analytics/average_lead_time.html', context)
        return render(request, 'analytics/average_lead_time.html', context)

    def _get_boards(self, user):
        boards = Board.objects.filter(is_active=True, author=user)
        return boards

    def _get_columns_list(self, board):
        columns_list = []
        col = board.first_column
        if col:
            while col:
                columns_list.append(col)
                if col.has_children():
                    children = col.get_all_children
                    for child in children:
                        columns_list.append(child)
                col = col.next_column
        return columns_list

    def _get_col_dict_from_list(self, col_list):
        dict = {}
        for i in range(len(col_list)):
            dict[col_list[i].name] = i
        return dict

    def _sort_cards_by_columns(self, card_history_list, cols_dict):
        """
        Sort cards by order of the columns on board
        """
        swapped = True
        unsorted = len(card_history_list)
        while swapped:
            swapped = False
            unsorted -= 1
            for i in range(unsorted):
                ch1 = card_history_list[i]
                ch2 = card_history_list[i+1]
                if cols_dict[ch1.column_to.name] > cols_dict[ch2.column_to.name]:
                    # swap elements i and i+1
                    ch_temp = card_history_list[i]
                    card_history_list[i] = card_history_list[i+1]
                    card_history_list[i+1] = ch_temp
                    swapped = True

        return card_history_list


class AverageLeadTimeChooseView(LoginRequiredMixin, View):
    def post(self, request):
        board_id = request.POST['board']
        context = {}
        boards = self._get_boards(request.user)

        context['boards'] = boards
        if board_id:
            board = get_object_or_404(Board, pk=board_id)
            projects = Project.objects.filter(is_active=True, board_id=board_id)
            context['selected_board'] = board
            context['projects'] = projects
            first_col = board.first_column
            if first_col and first_col.next_active_columns[:-1] is not None:
                context['start_columns'] = [first_col] + first_col.next_active_columns[:-1]
                if request.POST['start_column'] != 'all':
                    selected_start_column = board.column_set.filter(
                        pk=request.POST['start_column']).first()
                    context['selected_start_column'] = selected_start_column

                    # get only columns after start column
                    context['end_columns'] = selected_start_column.next_active_columns[:-1]

        return render(request, 'analytics/average_lead_time.html', context)

    def _get_boards(self, user):
        boards = Board.objects.filter(is_active=True, author=user)
        return boards


class AverageLeadTimeSelectView(LoginRequiredMixin, View):
    """
    Board is chosen and projects need to be filtered
    """
    def post(self, request):
        board_id = request.POST['board']
        context = {}
        boards = self._get_boards(request.user)

        context['boards'] = boards
        if board_id:
            board = get_object_or_404(Board, pk=board_id)
            projects = Project.objects.filter(is_active=True, board_id=board_id)
            context['selected_board'] = board
            context['projects'] = projects
            first_col = board.first_column
            if first_col and first_col.next_active_columns[:-1] is not None:
                context['start_columns'] = [first_col] + first_col.next_active_columns[:-1]

        return render(request, 'analytics/average_lead_time.html', context)

    def _get_boards(self, user):
        boards = Board.objects.filter(is_active=True, author=user)
        return boards


class AverageLeadTimeChooseEndView(LoginRequiredMixin, View):
    def post(self, request):
        board_id = request.POST['selected_board']
        context = {}
        boards = self._get_boards(request.user)
        context['boards'] = boards

        if board_id:
            board = get_object_or_404(Board, pk=board_id)
            projects = Project.objects.filter(is_active=True, board=board)
            context['selected_board'] = board
            context['projects'] = projects

            # fill start and end column fields
            first_col = board.first_column
            if first_col and first_col.next_active_columns[:-1] is not None:
                context['start_columns'] = [first_col] + first_col.next_active_columns[:-1]

                if request.POST['selected_start_column']:
                    selected_start_column = board.column_set.filter(
                        pk=request.POST['selected_start_column']).first()
                    context['selected_start_column'] = selected_start_column

                # get only columns after start column
                context['end_columns'] = selected_start_column.next_active_columns[:-1]

                if request.POST['stop_column']:
                    if request.POST['stop_column'] != 'all':
                        selected_end_column = board.column_set.filter(
                            pk=request.POST['stop_column']).first()
                        context['selected_end_column'] = selected_end_column

        return render(request, 'analytics/average_lead_time.html', context)

    def _get_boards(self, user):
        boards = Board.objects.filter(is_active=True, author=user)
        return boards